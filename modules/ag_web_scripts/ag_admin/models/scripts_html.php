<?php

if ($module_action_code == "list-web-scripts") {
    $element_table_web_scripts = array(
        "type" => "table",
        "text" => "Web Scripts",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "web_script",
            "background" => "#66ccff",
            "data_fields" => array(
                "title"=>array(
                    "title" => "Title",
                ),
            ),
//            "sql-query" => "select * from web_scripts"
        ),
    );

    array_push($module['elements'], $element_table_web_scripts);
}

if ($module_action_code == "ws_web_script") {
//    d($_GET);
    $render_method = "json";
    $table = 'web_scripts';
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "title", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_web_script") {
    // List authors
    $query = "select * from blog_authors;";
    $res_query = $sql->query($query);

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Web Script</h2>
    <hr>
  <div class="form-group">
    <label for="id_input_title">Title</label>
    <input name="title" type="text" class="form-control" id="id_input_title" aria-describedby="input_text" placeholder="Title of Web String" required>
  </div>
   <div class="form-group">
    <label for="id_input_description">Script</label>
    <textarea name="script" class="form-control" id="id_input_description" rows="3" ></textarea>
  </div>
  <div class="form-group">
    <label for="id_input_description">position</label>
    <br>
     <select name="position" class="custom-select">
      <option value="end_body">End Body ( default )</option>
      <option value="begin_body">Begin Body </option>
    </select>
  </div>
 
  
  <div class="form-group">
      <h5>Config</h5>
      <hr>
  </div>
  <div class="form-group">
    <label>Active</label>
    <div class="switch">
        <input name="bi_active" id="cmn-toggle-4" class="cmn-toggle cmn-toggle-round-flat" type="checkbox" >
        <label for="cmn-toggle-4"></label>
    </div>
  </div>
  
  <input name="ag_admin_action" value="new_web_script" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_delete_web_scripts") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            <div id_form="form_edit_web_scripts" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Info</div>
            <div id_form="form_delete_web_scripts" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Web String</h2>
                <hr>
              <input name="ag_admin_action" value="delete_web_script" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}

if ($module_action_code == "form_edit_web_script") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $query = "SELECT * FROM web_scripts where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        <div id_form="form_edit_web_scripts" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Info</div>
        <div id_form="form_delete_web_scripts" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Edit  Web Script</h2>
            <hr>
            <div class="form-group">
            <label for="id_input_title">Title</label>
            <input value="{$info['title']}" name="title" type="text" class="form-control" id="id_input_title" aria-describedby="input_text" placeholder="Title of Web String" required>
          </div>
           <div class="form-group">
            <label for="id_input_description">Script</label>
            <textarea name="script" class="form-control" id="id_input_description" rows="3" >{$info['script']}</textarea>
          </div>
          <div class="form-group">
            <label for="id_input_description">position</label>
            <br>
             <select name="position" class="custom-select">
              <option value="end_body">End Body ( default )</option>
              <option value="begin_body" {$selected_begin}>Begin Body </option>
            </select>
          </div>
         
          
         
          <div class="form-group">
            <label for="id_input_description">Active</label>
            <div class="switch">
                <input {$bi_active_html} name="bi_active" id="cmn-toggle-4" class="cmn-toggle cmn-toggle-round-flat" type="checkbox" >
                <label for="cmn-toggle-4"></label>
            </div>
          </div>
         
          
          
          
          <input name="ag_admin_action" value="edit_web_script" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="new_web_script"){


    $data=array(
        "title"=>$values["title"],
        "script"=>$values["script"],
        "position"=>$values["position"],
        "bi_active"=>$values["bi_active"],
    );

    // add images
    $array_json['data']=$data;
    $res = $ag_sql->insert("web_scripts",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("web_script",{$res});
JS;


    }



}
if($values["ag_admin_action"]=="edit_web_script"){


    $data=array(
        "title"=>$values["title"],
        "script"=>$values["script"],
        "position"=>$values["position"],
        "bi_active"=>$values["bi_active"],
    );

    $res = $ag_sql->update("web_scripts",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("web_script",{$values['id']});
JS;
    }
}


if($values["ag_admin_action"]=="delete_web_script"){
    $render_method = "json";
    $res = $ag_sql->delete("web_scripts"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}