<?php
namespace ag_web_scripts;

class web_script
{
    public $popups=array();
    
    private $type="";
    
    private $config_array;

    private $sql= null;
    // cdn
    
    /**
    * mail constructor.
    * @param $from
    * @param $path
    * @param $domain_mail_gun
    * @param $key_mailgun
    */
    public function __construct($config)
    {
        // load seccion ag_popups
        $this->config_array=$config;
        
        $this->type=$config['type'];

        $this->sql = \xeki\module_manager::import_module('ag_db_sql');

        
    }

    public function get_script_send_body(){
        $query = "select * from web_scripts where position ='end_body' and bi_active='on'";
        $result = $this->sql->query($query);
        $html="";
        foreach ($result as $item){
            $html.=$item['script'];
        }

        return $html;
    }
    public function get_scripts_begin_body(){
        $query = "select * from web_scripts where position ='begin_body' and bi_active='on'";
        $result = $this->sql->query($query);

        $html="";
        foreach ($result as $item){
            $html.=$item['script'];
        }

        return $html;
    }

    
    
}