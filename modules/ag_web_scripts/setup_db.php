<?php

require_once dirname(__FILE__).'/../_common/sql_lib.php';
require_once dirname(__FILE__).'/../../libs/xeki_util_methods.php';
require_once dirname(__FILE__).'/../../libs/xeki_core/module_manager.php';

## get main number of config db
$sql=\xeki\module_manager::import_module("ag_db_sql","main");


// user permissions
$categories = array(
    'table' => 'web_scripts',
    'elements' => array(
        'title' => 'text:NN:n:true:true:Title',
        'script' => 'textAreaBlog:NN:n:true:true:Script',
        'position' => 'text:NN:n:true:true:Script',


        'bi_active' => 'text:NN:n:true:true:active',
        'bi_date_creation' => 'date:NN:n:true:true:active',
        'bi_date_last_update' => 'date:NN:n:true:true:active',
    ),
);
createSqlxeki_v1($categories , $sql);

// user_permissions

// user_permissions_ref