<?php
## methods post
## recibe action post in xeki
$sql = \xeki\module_manager::import_module('ag_db_sql', 'main');
$ag_auth = \xeki\module_manager::import_module('ag_auth', 'main');
$popUp =  \xeki\module_manager::import_module('ag_popup');
//require_once dirname(__FILE__) . '/common/core_methods.php';
//require_once dirname(__FILE__) . '/common/mail.php';
$vl_action = $AG_ACTION = $_POST['AG_ACTION'];
$vl_values = $_POST;

//d("yes im running");

if ($vl_action == 'ag_auth::check_by_user') {
    $bool_check = $ag_auth->check_by_user($vl_values['user']);
    if ($bool_check){
        if($vl_values['url_post_login'])$ag_auth->go_to_login($vl_values['url_post_login']);
        else $ag_auth->go_to_login();
        }
    else{

        if($vl_values['url_post_login'])$ag_auth->go_to_register($vl_values['url_post_login']);
        else $ag_auth->go_to_register();
        }
} else if ($vl_action == 'ag_auth::resubmit_confirm') {
    $info = $ag_auth->get_user_info();
    $email=$info['email'];
    $code=$info['confirm_code'];

    $ag_auth->send_email_confirm_account($email,$code,$info);


} else if ($vl_action == 'ag_auth::login') {

    if(!isset($vl_values['ag_next_page']))
        $ag_auth->set_logged_page($vl_values['ag_next_page']);


    if(!isset($vl_values['user_identifier']))
        \xeki\core::fatal_error("Not found user_identifier on post request");

    if(!isset($vl_values['user_pass']))
        \xeki\core::fatal_error("Not found user_pass on form");

    $ag_auth->login($vl_values['user_identifier'], $vl_values['user_pass']);

    // solo pasa si no se logea
    $message_popup = $ag_auth->get_value_param("msg_no_valid_user");
    $popUp->add_msg( $message_popup );

}

else if ($vl_action == 'ag_auth::register') {

    ## check if email is register
    if(!isset($vl_values['email'])){
        if(isset($vl_values['user_identifier']))$vl_values['email']=$vl_values['user_identifier'];
    }

    # no password partial register
    if(!isset($vl_values['user_pass'])){
        $vl_values['user_pass']="password_ag_user";
        $vl_values['tp_ag_auth_state_user']="partial";
    }
    else{
        $vl_values['tp_ag_auth_state_user']="complete";
    }

    // valid

    if(!isset($vl_values['ag_next_page'])){
        $ag_auth->set_logged_page($vl_values['ag_next_page']);
    }

    // create extra data from form
    $extra_data= array();
    foreach ($vl_values as $key=>$value){
        if (strpos($key, 'tp_') !== false) {
            $key_fix = str_replace('tp_','',$key);
            $extra_data[$key_fix]=$value;
        }
    }

    ## valid if user exist

    $query = "SELECT * FROM " . $ag_auth->getTableUser() . " WHERE " . $ag_auth->getFieldUser() . "='" . $vl_values['email'] . "'";
    $res = $sql->query($query);

    if (count($res) == 0) {
        $res = $ag_auth->secure_register($vl_values['email'], $vl_values['user_pass'], $extra_data);
        $ag_auth->login($vl_values['email'], $vl_values['password']);


        $message_popup = $ag_auth->get_value_param("msg_new_user");
        $popUp->add_msg( $message_popup );
    } else {

        $popUp->add_msg("Ya existe una cuenta asociada a este correo.");
        $message_popup = $ag_auth->get_value_param("msg_user_exist");
        $popUp->add_msg( $message_popup );

        // TODO re do partial register



    }


}

## ---------------------------------
## Update Data
## ---------------------------------

else if ($vl_action == 'ag_auth::update_edit') {
    $data = array(
    'name' => limpiarCadena($vl_values['name']),
    'lastName' => limpiarCadena($vl_values['lastName']),
    'cellphone' => limpiarCadena($vl_values['cellphone']),
    'birthDate' => limpiarCadena($vl_values['birthDate']),
    );
    $USER = $ag_auth->getUserInfo();;

    $res = $sql->update($ag_auth->getTableUser(), $data, 'id="' . $USER["id"] . '"');
    $ag_auth->updateUserInfo();
    //    ag_pushMsg('Tu cuenta ha sido actualiada :)');
    // echo '<meta http-equiv="refresh" content="0">';
}

## ---------------------------------
## Update Data Pass
## ---------------------------------

else if ($vl_action == 'ag_auth::register_pass') {
    $data = array(
        'password' => $ag_auth->encrypt($vl_values['user_pass']),
        'ag_auth_state_user' =>"complete",
    );
    $USER = $ag_auth->getUserInfo();
    $res = $sql->update($ag_auth->getTableUser(), $data, 'id="' . $USER["id"] . '"');

    $ag_auth->updateUserInfo();
    if(isset($vl_values['AG_NEXT_PAGE'])){
        ag_redirect($vl_values['AG_NEXT_PAGE']);
    }

    //    ag_pushMsg('Tu contraseña ha sido actualizada');

}

else if ($vl_action == 'ag_auth::update_edit_pass') {
    $data = array(
        'password' => encrypt($vl_values['password']),
    );
    $USER = $ag_auth->getUserInfo();
    $res = $sql->update($ag_auth->getTableUser(), $data, 'id="' . $USER["id"] . '"');
    //    ag_pushMsg('Tu contraseña ha sido actualizada');
    $popUp->add_msg("Tu contraseña ha sido actualizada.");
}

else if ($vl_action == 'ag_auth::recover_pass_update') {
    $response = $ag_auth->set_pass_by_code_recover($_SESSION['recover_code'],$vl_values['user_pass']);
    //    ag_pushMsg('Tu contraseña ha sido actualizada');

    if($response){
        $popUp->add_msg("Tu contraseña ha sido actualizada.");
    }
    else{
        $popUp->add_msg("El codigo de actualizacion ya no es valido, intenta recuperar de nuevo.");
    }



}

## ---------------------------------
## Set Recover pass
## ---------------------------------

else if ($vl_action == 'ag_auth::recover_pass') {

    $res = $ag_auth->set_code_recover($vl_values['email']);

    $mail = \xeki\module_manager::import_module("ag_mail");

  	$recover_pass_mail_route = $ag_auth->get_value_param("mail_recover_pass_route");

  	$path_file = dirname(__FILE__) . "/../../../$recover_pass_mail_route";

  	if(!file_exists($path_file)){
  		$path_file = dirname(__FILE__)."/pages/mail/recover_pass.html";
  	}

    // TODO mejorar esto
    // get ifo user
    $info_to_email = $ag_auth->get_info_by_email($vl_values['email']);
      //d($vl_values);
      //d($info_to_email );
      //die();
    $content = file_get_contents($path_file);
    //    d($AG_BASE_COMPLETE.$AG_L_PARAM.'/'.$res);

    $info_to_email['url']=$AG_BASE_COMPLETE.$AG_L_PARAM.'/'.$res;
    //d($info_to_email);
    //d($content);
    //die();
    $mail->send_email($vl_values['email'],"Recover Pass",$content,$info_to_email);
    //    ag_pushMsg('Tu contraseña ha sido actualizada');
    $popUp->add_msg("Se ha enviado a tu correo las instrucciones.");
}

else if($vl_action == 'ag_auth::confirm_account_resend_email') {

}