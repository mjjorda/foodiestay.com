<?php
// d("hiii!!");
// d($AG_HTTP_REQUEST);
$user_zone = \xeki\module_manager::import_module('ag_auth');
$enable_controllers = $user_zone->get_value_param("use_module_controllers");
// d($enable_controllers);
if($enable_controllers){

    ## load urls for config file
    $login_url=$user_zone->get_value_param("login_page_url");
    $register_url=$user_zone->get_value_param("register_page_url");
    $logout_url=$user_zone->get_value_param("logout_page_url");
    $recover_url=$user_zone->get_value_param("recover_pass_page_url");

    $confirm_account_route_url=$user_zone->get_value_param("confirm_account_route_url");



    // d($login_url);
    // d($register_url);
    // d($logout_url);
    // d($recover_url);
    

    $AG_HTTP_REQUEST->register_url("$login_url", 'auth_login', "ag_auth");
    $AG_HTTP_REQUEST->register_url("$login_url", 'auth_login', "ag_auth");


    $AG_HTTP_REQUEST->register_url("$register_url", 'auth_register', "ag_auth");
    $AG_HTTP_REQUEST->register_url("$register_url/", 'auth_register', "ag_auth");

    $AG_HTTP_REQUEST->register_url("$logout_url", 'auth_logout', "ag_auth");
    $AG_HTTP_REQUEST->register_url("$logout_url/", 'auth_logout', "ag_auth");

    $AG_HTTP_REQUEST->register_url("$recover_url", 'auth_recover_pass', "ag_auth");
    $AG_HTTP_REQUEST->register_url("$recover_url/" ,'auth_recover_pass', "ag_auth");

    $AG_HTTP_REQUEST->register_url("$recover_url/[*:id]", 'auth_recover_pass_update', "ag_auth");


    // confirm account
    $AG_HTTP_REQUEST->register_url("$confirm_account_route_url", 'auth_confirm_account', "ag_auth");
    $AG_HTTP_REQUEST->register_url("$confirm_account_route_url/[*:id]", 'auth_confirm_account', "ag_auth");





    // facebook
    $facebook_login=$user_zone->get_value_param("facebook_login");
    if($facebook_login){
        $facebook_auth_page=$user_zone->get_value_param("facebook_auth_page");
        $AG_HTTP_REQUEST->register_url("$facebook_auth_page", 'auth_facebook', "ag_auth");
        $AG_HTTP_REQUEST->register_url("$facebook_auth_page/", 'auth_facebook', "ag_auth");

        $facebook_call_back_url=$user_zone->get_value_param("facebook_call_back_url");
        $AG_HTTP_REQUEST->register_url("$facebook_call_back_url", "auth_facebook_callback", "ag_auth");
        $AG_HTTP_REQUEST->register_url("$facebook_call_back_url/", "auth_facebook_callback", "ag_auth");
    }
}
