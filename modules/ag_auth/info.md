# Auth Module

Auth module is a autentication module for xeki framework :D.

## Quick Start

Initialize db run module set up 

```bash
php modules/module_set_up.php
```

##### Copy folder config

```bash
mkdir core/modules_config/ag_auth/
cp modules/ag_auth/config.php core/modules_config/ag_auth/
```

##### Copy template folder
```bash
mkdir core/modules_config/ag_auth/
cp modules/ag_auth/config.php core/modules_config/ag_auth/
```


##### Activate urls of module 

```php
$_ARRAY_MODULES_TO_LOAD_URLS = array(
    "....",
    "....",
    "ag_auth",
); 
```

For add info to render info 

$_ARRAY_RUN_START = array(
    #modules_names
   'ag_auth'
);


## Use in controllers
We can use this methods for create secure zones in you implementation.

#### General Example use
```php
// import module 
$ag_auth =  \xeki\module_manager::import_module('ag_auth');
// check if is logged, if is not logged is redirected to login page
$ag_auth->check_logged();
```

#### Advance Example use
```php
// import module 
$ag_auth =  \xeki\module_manager::import_module('ag_auth');

// set name space of auth
$ag_auth->set_name_space("my_name_space_application");

// set login page
$user_zone->set_logged_page("user/my_login_page");

// check if is logged, if is not logged is redirected to login page
$ag_auth->check_logged();

if($ag_auth->is_logged()){
    // do something
}

// check if have permision for some action
$ag_auth->check_permission('my_action_code');

if($ag_auth->have_permission()){
    // do something
}

```

#### All Methods
##### Import module

```php
$ag_auth =  \xeki\module_manager::import_module('ag_auth');
```

##### Check Auth
```php

// if not login redirect to login page

$user_zone->set_logged_page("my login custom");
$ag_auth->check_logged();

// just return login or not 
$is_auth = $ag_auth->check_auth();

```

##### Check by Name spaces
We can have diferentes name spaces for each system or 
```php

$ag_auth->set_name_space("my_name_space_application");

$ag_auth->check_logged();

$ag_auth->check_permission('my_action_code');


```


##### Check Action Permision
```php

// if not login redirect to not permision page
$ag_auth->check_logged();

// just return login or not 
$is_auth = $ag_auth->check_auth("action_permission_code");

```

##### Other methods

Get info of user
```php
$user_info = $ag_auth->get_user_info();

$user_info = $ag_auth->get_persmissions_info();
```


