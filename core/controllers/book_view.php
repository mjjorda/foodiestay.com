<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo
$ag_auth = \xeki\module_manager::import_module("ag_auth");

//$query = "SELECT * from slider";
//$slider_home = $sql->query($query);
//$slider_home= $slider_home[0];
// d($slider_home);
//
//$query = "SELECT * from slider order by order_list asc";
//$slider = $sql->query($query);
//$AG_HTML->set_seo($title, $description, true);

$params = \xeki\core::$URL_PARAMS_LAST;
$items = explode("-",$params);
$id = $items[count($items)-1];
$type = $items[count($items)-2];

//d($id);
//d($type);

$info_book = false;
$type_book = "";

$query = "SELECT * from books where id='{$id}'";
$info_book = $sql->query($query);
$type_book = "books_experience";


//d($info_book);
// if not exist

if(count($info_book)>0){

    $info_book = $info_book[0];

    // get related books
    $query = "SELECT * from books where active = 'on' order by rand() limit 3";
    $books = $sql->query($query);

    $query = "SELECT * from places where active = 'on'";
    $places = $sql->query($query);
    $places_id = array();
    foreach ($places as $key => $item){
        $places_id[$item['id']]=$item;
    }


    foreach ($books as $key => $item){
        $books[$key]['country_name'] = $places_id[$item['place']]['name'];

        if($item['date_end']=='0000-00-00'){
            $books[$key]['n_days']= 1;
        }
        else{
            $now = time(); // or your date as well
            $date_ini = strtotime($item['date_ini']);
            $date_end = strtotime($item['date_end']);
            $datediff = $date_end - $date_ini;

            $books[$key]['n_days'] = round($datediff / (60 * 60 * 24));
        }

        // hours
        $hour_ini = $item['hour_ini'];
        $hour_ini = explode(":",$hour_ini)[0];
        $hour_end = $item['hour_end'];
        $hour_end = explode(":",$hour_end)[0];

        $hours = $item['hour_end']-$item['hour_ini'];
        $books[$key]['hours']= $hours;
        // calculate days
//    d($item);
    }



    // get apartments
    $apartments= array();
    $query = "SELECT * from apartments_ref,apartments where
                apartments_ref.id_book='{$info_book['id']}' and 
                apartments_ref.`id_apartment`=apartments.id";
    $apartments = $sql->query($query);
    // 
    foreach($apartments as $key=>$item){
        $apartments[$key]["images_list"]=json_decode($item['images_array'],true);
    }
    $info_book["list_images"]=json_decode($info_book["list_images"],true);

//    d($info_book["list_images"]);

    // get country
    $query = "SELECT * from places where id='{$info_book['place']}'";
    $place = $sql->query($query);
    $place = $place[0];

    // get city
    $query = "SELECT * from cities where id='{$info_book['city']}'";
    $city = $sql->query($query);
    $city = $city[0];



    $country = $place['name'];
//    d($info_book);
    // for plans inners
    if($info_book['type_book']=="plans"){
        $inners_days = $info_book['json_inners_items'];
        $inners_days = json_decode($inners_days ,true);
        $info_book['inners_days'] = array();
        foreach ($inners_days as $inner_day ){
            $query = "SELECT * FROM books where id ='{$inner_day}'";
            $info = $sql->query($query);
            $info = $info[0];

            $info["list_images"]=json_decode($info["list_images"],true);

            $info["list_image"]=json_decode($info,true);
            array_push($info_book['inners_days'],$info);
        }
    }

    if($info_book['date_end']=='0000-00-00'){
        $info_book['n_days']= 1;
    }
    else{
        $now = time(); // or your date as well
        $date_ini = strtotime($info_book['date_ini']);
        $date_end = strtotime($info_book['date_end']);
        $datediff = $date_end - $date_ini;

        $info_book['n_days'] = round($datediff / (60 * 60 * 24));
    }

    // hours
    $hour_ini = $info_book['hour_ini'];
    $hour_ini = explode(":",$hour_ini)[0];
    $hour_end = $info_book['hour_end'];
    $hour_end = explode(":",$hour_end)[0];

    $hours = $info_book['hour_end']-$info_book['hour_ini'];
    $info_book['hours']= $hours;


    // get saved placed
    $saved = false;
    if($ag_auth->check_auth()){
        $info_user = $ag_auth->get_user_info();
        $query = "SELECT * from user_saved_books where user_ref='{$info_user['id']}' and book_ref='{$info_book['id']}'";
        $saved = $sql->query($query);
        if(count($saved)>0){
            $saved = false;
        }
    }

    $title = $info_book['name'];
    $description = $info_book['description'];

    $AG_HTML->set_seo($title, $description, true);


    // save view book
    $items_to_print = array();
//    d($info_book[0]);
    $items_to_print['active_page']="book";
    $items_to_print['book']  = $info_book ;
    $items_to_print['city']  = $city ;
    $items_to_print['country']  = $country ;
    $items_to_print['books'] = $books;
    $items_to_print['type_book']=$type_book;
    $items_to_print['apartments'] = $apartments;


//    d($items_to_print);
    $AG_HTML->render('book_view.html', $items_to_print);
}

// get book

