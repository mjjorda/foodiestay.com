<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$title = "Home";
$description = "";

$AG_HTML->set_seo($title, $description, true);

//$query = "SELECT * from slider";
//$slider_home = $sql->query($query);
//$slider_home= $slider_home[0];
// d($slider_home);
//
//$query = "SELECT * from slider order by order_list asc";
//$slider = $sql->query($query);
//$AG_HTML->set_seo($title, $description, true);


// script
$ag_auth = \xeki\module_manager::import_module("ag_auth");
if(!$ag_auth->check_auth()){
    \xeki\core::redirect("login");
}

$info = $ag_auth->get_user_info();
$query = "
select * from 
  books, user_saved_books
where 
    books.id=user_saved_books.book_ref 
    and  '{$info['id']}'=user_saved_books.user_ref 
    and books.active ='on'
";

$books = $sql->query($query,true);



// country list
$query = "SELECT * from places where active = 'on'";
$places = $sql->query($query);
$places_id = array();
foreach ($places as $key => $item){
    $places_id[$item['id']]=$item;
}


foreach ($books as $key => $item){

    $books[$key]=$books[$key]['books'];
    $books[$key]['country_name']=$places_id[$item['place']]['name'];
    $books[$key]['saved']=true;

    if($item['date_end']=='0000-00-00'){
        $books[$key]['n_days']= 0;
    }
    else{
        $now = time(); // or your date as well
        $date_ini = strtotime($item['date_ini']);
        $date_end = strtotime($item['date_end']);
        $datediff = $date_end - $date_ini;

        $books[$key]['n_days'] = round($datediff / (60 * 60 * 24));

    }
    // calculate days
//    d($item);
}


$items_to_print = array();
$items_to_print['active_page']="account";
$items_to_print['books']=$books;
$AG_HTML->render('user_books_save.html', $items_to_print);