<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$title = "Reserves";
$description = "";

$AG_HTML->set_seo($title, $description, true);


// script
$ag_auth = \xeki\module_manager::import_module("ag_auth");
if(!$ag_auth->check_auth()){
    \xeki\core::redirect("login");
}

$id_reserve = \xeki\core::$URL_PARAMS_LAST;

$info = $ag_auth->get_user_info();
$query = "
select * from 
books, user_buy
where books.id=user_buy.book_ref 
and '{$info['id']}'= user_buy.user_ref
and '{$id_reserve}'= user_buy.id ";

$info_book = $sql->query($query,true);

if(count($info_book)>0) {
    $info_book=$info_book[0];
//    d($info_book);

    $apartment=false;
    if($info_book['user_buy']['apartment_ref']!=-1) {
        $apartment_id = $info_book['user_buy']['apartment_ref'];
        $query = "SELECT * from apartments where id='{$apartment_id}'";
        $res = $sql->query($query);
        $apartment = $res[0];
    }
//    d($apartment);

// country list
    $query = "SELECT * from places where active = 'on'";
    $places = $sql->query($query);
    $places_id = array();
    foreach ($places as $key => $item) {
        $places_id[$item['id']] = $item;
    }

    $items_to_print = array();
    $items_to_print['active_page']="account";

    $items_to_print['book']=$info_book['books'];
    $items_to_print['user_buy']=$info_book['user_buy'];
    $items_to_print['apartment']=$apartment;
    $AG_HTML->render("user_books_own_view.html", $items_to_print);
}

