<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$title = "Reserves";
$description = "";

$AG_HTML->set_seo($title, $description, true);

//$query = "SELECT * from slider";
//$slider_home = $sql->query($query);
//$slider_home= $slider_home[0];
// d($slider_home);
//
//$query = "SELECT * from slider order by order_list asc";
//$slider = $sql->query($query);
//$AG_HTML->set_seo($title, $description, true);


// script
$ag_auth = \xeki\module_manager::import_module("ag_auth");
if(!$ag_auth->check_auth()){
    \xeki\core::redirect("login");
}

$info = $ag_auth->get_user_info();
$query = "
select * from 
books, user_buy
where books.id=user_buy.book_ref 
and '{$info['id']}'=user_buy.user_ref ";

$books = $sql->query($query,true);

// country list
$query = "SELECT * from places where active = 'on'";
$places = $sql->query($query);
$places_id = array();
foreach ($places as $key => $item){
    $places_id[$item['id']]=$item;
}


foreach ($books as $key => $item){

    $final_full_price = $books[$key]['user_buy']['final_full_price'];
    $state_payment = $books[$key]['user_buy']['state_payment'];
    $id_reserve = $books[$key]['user_buy']['id'];
    $state_payment_txt='En espera';

    $books[$key]=$books[$key]['books'];
    $books[$key]['final_full_price']=$final_full_price;
    $books[$key]['id_reserve']=$id_reserve;


//    Rechazado, pagado, pendiente, espera, cancelada)
    if($state_payment=="confirmed_payment"){
        $state_payment_txt="Pagado";
    }
    else{
        $state_payment_txt="Rechazado";
    }
    $books[$key]['state_payment']=$state_payment_txt;
    $books[$key]['country_name']=$places_id[$item['place']]['name'];

    if($item['date_end']=='0000-00-00'){
        $books[$key]['n_days']= 0;
    }
    else{
        $now = time(); // or your date as well
        $date_ini = strtotime($item['date_ini']);
        $date_end = strtotime($item['date_end']);
        $datediff = $date_end - $date_ini;

        $books[$key]['n_days'] = round($datediff / (60 * 60 * 24));

    }
    // calculate days
//    d($item);
}

$items_to_print = array();
$items_to_print['active_page']="account";
$items_to_print['books']=$books;
$AG_HTML->render('user_books_own.html', $items_to_print);