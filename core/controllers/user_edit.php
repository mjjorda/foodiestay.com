<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$title = "Home";
$description = "";

$AG_HTML->set_seo($title, $description, true);

//$query = "SELECT * from slider";
//$slider_home = $sql->query($query);
//$slider_home= $slider_home[0];
// d($slider_home);
//
//$query = "SELECT * from slider order by order_list asc";
//$slider = $sql->query($query);
//$AG_HTML->set_seo($title, $description, true);


// script
$ag_auth = \xeki\module_manager::import_module("ag_auth");
$ag_config = \xeki\module_manager::import_module("ag_config");

if(!$ag_auth->check_auth()){
    \xeki\core::redirect("login");
}

$list_langs = $ag_config->get_lang_list();
$user_info = $ag_auth->get_user_info();

$html_languages = "";
foreach ($list_langs as $key => $value) {
    $selected = $user_info['lang']==$value['code_lang']?"selected":"";
    $html_languages .= "<option value='{$value['code_lang']}' {$selected}>{$value['name']}</option>";
}


$items_to_print = array();
$items_to_print['active_page']="account";
$items_to_print['books']=$us;
$items_to_print['html_languages']=$html_languages;
$AG_HTML->render('user_edit.html', $items_to_print);