<?php

$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$payment_id = $_GET['paymentId'];
$token = $_GET['token'];
$payer_id = $_GET['PayerID'];


//get sql
$query = "select * from user_buy where pay_id_paypal='{$payment_id}' ";
$reserve = $sql->query($query);
$reserve = $reserve[0];

$query = "select * from user where id='{$reserve['user_ref']}' ";
$user = $sql->query($query);
$user = $user[0];
// mails for testing
//d($reserve);
//d($user);

if ($user['email'] == 'liuspatt@gmail.com' || $user['email'] == 'networkarts@gmail.com' || $user['email'] == 'info@foodiestay.com') {
    confirm_payment($payment_id);

}

function confirm_payment($payment_id)
{
    $mail = \xeki\module_manager::import_module("ag_mail");
    $sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo
    // update data base
    $data = array(
        "state_payment" => "confirmed_payment",
        "state" => "to_confirm",
    );

    $sql->update("user_buy", $data, "pay_id_paypal='{$payment_id}'");

    // send email


    // get reserve
    $query = "select * from user_buy where pay_id_paypal='{$payment_id}' ";
    $reserve = $sql->query($query);
    $reserve = $reserve[0];

    // get book
    $query = "select * from books where id='{$reserve['book_ref']}' ";
    $book = $sql->query($query);
    $book = $book[0];


    // get book
    $query = "select * from cities where id='{$reserve['city']}' ";
    $city = $sql->query($query);
    $city = $city[0];

    // get book
    $query = "select * from places where id='{$book['place']}' ";
    $place = $sql->query($query);
    $place = $place[0];

    // get user
    $query = "select * from user where id='{$reserve['user_ref']}' ";
    $user = $sql->query($query);
    $user = $user[0];


    // email

    $path_file = dirname(__FILE__) . "/../../pages/mails/payment_confirm.html";
    $content = file_get_contents($path_file);
    $info_to_email=array(
        "user_name"=>$user['name'],
        "place_name"=>$place['name'],
        "n_persons"=>$reserve['number_persons'],
        "date_ini"=>$book['date_ini'],
        "hour_ini"=>$book['hour_ini'],
        "city"=>$city['name'],
        "id_reserve"=>$reserve['id_reserve'],
    );
    $mail->send_email($user['email'],"Success book",$content,$info_to_email);
//    d($place);
//    d($user);



    \xeki\core::redirect("reserve-success");
    // redirect :)

}

function cancel_payment($payment_id)
{
    $sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo
    // update data base
    $data = array(
        "state_payment" => "declined",
        "state" => "declined",
    );

    $sql->update("user_buy", $data, "pay_id_paypal='{$payment_id}'");

    // redirect :)
    \xeki\core::redirect("reserve-fail");

}

die();


//d($info_book['price']);
//d($price);

//Credencial Firma
//Nombre de usuario de API
//info_api1.foodiestay.com
//Ocultar
//Contraseña de API
//QQW5QUY7RGHGCF3C
//Ocultar
//Firma
//AovsH2aee5eitzsHblVL4jqRvmJLAxoa8tkd7SDl0DfLBW-g0WyvOhXG
//Ocultar

// sandbox

$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
        'AfRzKsC_2nHxsUniRYB1XQB_Kd1MtOE_wSXN8okRtvdZt51uFGY2tNL-m18MwrL2iVVQezlfVhyNVnm-',     // ClientID
        'EI7Epr8X_YmGRVpUzAP_dOFvvZCkwZ0Wj1xm-0q0GUnSWtZVZaxXuXCAArdXAzuIj5ItJJeLKYWHzaxd'      // ClientSecret
    )
);

// production
$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
        'ASpEttLztCYE5bxPKC0LNpb1YQTUgE9nqyj5ZOUxu3ZMBAlV9du46iX4GMS17yzJewi6vSABXYsqRJG0',     // ClientID
        'EDvtTcyH4UHnDvkZZacT09okoxFMTXA470ZgHObhtbzlFR-Ii1qjrAVhnOpw7FDZu_VO_yQdY5THJjVi'      // ClientSecret
    )
);
$apiContext->setConfig(
    array(
        'mode' => 'live',
    )
);

$paymentId = $_GET['paymentId'];
$payment = Payment::get($paymentId, $apiContext);
// ### Payment Execute
// PaymentExecution object includes information necessary
// to execute a PayPal account payment.
// The payer_id is added to the request query parameters
// when the user is redirected from paypal back to your site
$execution = new PaymentExecution();
$execution->setPayerId($_GET['PayerID']);
// ### Optional Changes to Amount
// If you wish to update the amount that you wish to charge the customer,
// based on the shipping address or any other reason, you could
// do that by passing the transaction object with just `amount` field in it.
// Here is the example on how we changed the shipping to $1 more than before.
$transaction = new Transaction();
$amount = new Amount();
$details = new Details();
$details->setShipping(0)
    ->setTax(0)
    ->setSubtotal(2);
$amount->setCurrency('EUR');
$amount->setTotal(2);
$amount->setDetails($details);
$transaction->setAmount($amount);
// Add the above transaction object inside our Execution object.
$execution->addTransaction($transaction);

try {
    // Execute the payment
    // (See bootstrap.php for more on `ApiContext`)
    $result = $payment->execute($execution, $apiContext);
    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
    d("Executed Payment");
    d("Payment");
    d($payment->getId());
    d($execution);
    d($result);
    try {
        $payment = Payment::get($paymentId, $apiContext);
        confirm_payment($payment_id);
    } catch (Exception $ex) {
        cancel_payment($payment_id);
        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        d("Get Payment");
        d("Payment");
        d($ex);
        exit(1);
    }
} catch (Exception $ex) {
    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
    d("Executed Payment");
    d("Payment");
    d($ex);
    cancel_payment($payment_id);

    exit(1);
}
// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
d("Get Payment");
d("Payment");
d($payment->getId());
d($payment);
