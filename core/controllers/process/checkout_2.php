<?php
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
d($_SESSION['buy_process']);
$sql = \xeki\module_manager::import_module("ag_db_sql");// me importa un modulo

$title = "Checkout";
$description = "";


$info_book = array();
$type_book = $_SESSION['buy_process']['type_book'];
$id = $_SESSION['buy_process']['id_book'];
$persons = $_SESSION['buy_process']['persons'];

$apartments_id = $_SESSION['buy_process']['apartments'];

$query = "SELECT * from books where id='{$id}'";
$info_book = $sql->query($query);
$info_book = $info_book[0];

$persons = $_SESSION['buy_process']['persons']-$info_book['base_price_persons'];

// load info and save in db
$final_price = 0;
// price book

$final_price = $final_price + $info_book['price'];
d("price_product");
d($final_price);
// price additional persons
d($info_book['add_person_price']);
$final_price+=$persons*$info_book['add_person_price'];
d("price_persons");
d($final_price);

// price segure cancellation
if($_SESSION['buy_process']['cancellation']=="on"){
    d("price_cancellation");
    $final_price+=$info_book['cancellation_person_price']*$persons;
}
else{
    $info_book['cancellation_person_price']=0;
}
d($final_price);

// private
if($_SESSION['buy_process']['private']=="on"){
    d("price_private");
    $final_price+=$info_book['private_price'];
}
else{
    $info_book['private_price']=0;
}
d($final_price);
$query = "SELECT * from apartments where id='{$apartments_id}'";
$info_apartment = $sql->query($query);
$info_apartment  = $info_apartment[0];

// apartment
if($_SESSION['buy_process']['apartment']=="on"){
    d("price_apartment");
    $final_price+=$info_apartment['price'];
}
else{
    $info_apartment=array();
    $info_apartment['price']=0;
}

d($final_price);
//  calculate price

// save info
$query = "SELECT * from user_buy where id='{$_SESSION['buy_process']['reserve']}'";
$res = $sql->query($query);
$reserve = $res[0];

$data = array(
    "cancellation"=>$_SESSION['buy_process']['cancellation'],
    "price_cancellation"=>$info_book['cancellation_person_price']*$persons,
    "apartment"=>$_SESSION['buy_process']['apartment'],
    "price_apartment"=>$info_apartment['price'],
    "private"=>$_SESSION['buy_process']['private'],
    "price_private"=>$info_book['private_price'],
    "final_full_price"=>$final_price,
);
$sql->update("user_buy",$data,"id='{$reserve['id']}'");

//d($info_book);

//d($info_book['price']);
//d($price);

//Credencial Firma
//Nombre de usuario de API
//info_api1.foodiestay.com
//Ocultar
//Contraseña de API
//QQW5QUY7RGHGCF3C
//Ocultar
//Firma
//AovsH2aee5eitzsHblVL4jqRvmJLAxoa8tkd7SDl0DfLBW-g0WyvOhXG
//Ocultar







// sandbox

$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
        'AfRzKsC_2nHxsUniRYB1XQB_Kd1MtOE_wSXN8okRtvdZt51uFGY2tNL-m18MwrL2iVVQezlfVhyNVnm-',     // ClientID
        'EI7Epr8X_YmGRVpUzAP_dOFvvZCkwZ0Wj1xm-0q0GUnSWtZVZaxXuXCAArdXAzuIj5ItJJeLKYWHzaxd'      // ClientSecret
    )
);

// production
$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
        'ASpEttLztCYE5bxPKC0LNpb1YQTUgE9nqyj5ZOUxu3ZMBAlV9du46iX4GMS17yzJewi6vSABXYsqRJG0',     // ClientID
        'EDvtTcyH4UHnDvkZZacT09okoxFMTXA470ZgHObhtbzlFR-Ii1qjrAVhnOpw7FDZu_VO_yQdY5THJjVi'      // ClientSecret
    )
);
$apiContext->setConfig(
    array(
        'mode' => 'live',
    )
);
// After Step 2
$payer = new \PayPal\Api\Payer();
$payer->setPaymentMethod('paypal');

$amount = new \PayPal\Api\Amount();
//$amount->setTotal('1.00');
$amount->setTotal($final_price);
//$amount->setTotal('1.00');
$amount->setCurrency('EUR');

$item1 = new Item();
$item1->setName($info_book['name'])
    ->setCurrency('EUR')
    ->setQuantity(1)
    ->setPrice($final_price);
$itemList = new ItemList();
$itemList->setItems(array($item1));

$transaction = new \PayPal\Api\Transaction();
$transaction->setItemList($itemList);
$transaction->setAmount($amount);
$transaction->setDescription($info_book['name']);

$redirectUrls = new \PayPal\Api\RedirectUrls();
$redirectUrls->setReturnUrl("http://foodiestay.com/payment/paypal/accept")
    ->setCancelUrl("http://foodiestay.com/payment/paypal/cancel");

$redirectUrls->setReturnUrl("https://foodiestay.com/payment/paypal/accept")
    ->setCancelUrl("https://foodiestay.com/payment/paypal/cancel");

$payment = new \PayPal\Api\Payment();
$payment->setIntent('sale')
    ->setPayer($payer)
    ->setTransactions(array($transaction))
    ->setRedirectUrls($redirectUrls);

try {
    $payment->create($apiContext);
    d($payment->getId());

    $data = array(
        "pay_id_paypal"=>$payment->getId(),
    );
    $sql->update("user_buy",$data,"id='{$reserve['id']}'");
//    die();
//    echo $payment;
    \xeki\core::redirect($payment->getApprovalLink());

    echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
}
catch (\PayPal\Exception\PayPalConnectionException $ex) {
    // This will print the detailed information on the exception.
    //REALLY HELPFUL FOR DEBUGGING
    echo $ex->getData();
}

die();