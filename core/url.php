<?php
$AG_HTTP_REQUEST->register_url('/', 'home');
$AG_HTTP_REQUEST->register_url('main', 'home');


$AG_HTTP_REQUEST->register_url('places', 'place_list');
$AG_HTTP_REQUEST->register_url('places/', 'place_list');
$AG_HTTP_REQUEST->register_url('places/[*:id]', 'place_view');


$AG_HTTP_REQUEST->register_url('payment/paypal/accept', 'process/callback');
$AG_HTTP_REQUEST->register_url('payment/paypal/cancel', 'process/paypal_cancel');


$AG_HTTP_REQUEST->register_url('reserve-success', 'plain/reserve_success');
$AG_HTTP_REQUEST->register_url('reserve-fail', 'plain/reserve_fail');


$AG_HTTP_REQUEST->register_url('books', 'book_list');
$AG_HTTP_REQUEST->register_url('books/', 'book_list');

$AG_HTTP_REQUEST->register_url('books/[*:id]', 'book_view');
// check not_controller pages
$AG_HTTP_REQUEST->register_url('object/[*:id]', 'object_view');
$AG_HTTP_REQUEST->register_url('contact', 'contact');
$AG_HTTP_REQUEST->register_url('about', 'plain/about');


$AG_HTTP_REQUEST->register_url('legal/terms', 'legal/terms');
$AG_HTTP_REQUEST->register_url('legal/privacy', 'legal/privacy');

$AG_HTTP_REQUEST->register_url('book-process', 'process/book_process');

$AG_HTTP_REQUEST->register_url('checkout', 'process/checkout_1');
$AG_HTTP_REQUEST->register_url('checkout2', 'process/checkout_2');

$AG_HTTP_REQUEST->register_url('user-books-own', 'user_books_own');
$AG_HTTP_REQUEST->register_url('user-books-save', 'user_books_save');


$AG_HTTP_REQUEST->register_url('activate-account', 'process/checkout_2');


$AG_HTTP_REQUEST->register_url('account', 'user_edit');
$AG_HTTP_REQUEST->register_url('reserves/[*:id]', 'user_books_own_view');

//$AG_HTTP_REQUEST->register_url('check_out', 'process/checkout');

//$AG_HTTP_REQUEST->register_url('login', 'login');

//$AG_HTTP_REQUEST->register_url('register', 'register');

//$AG_HTTP_REQUEST->register_url('regex', 'controller','module=core');


