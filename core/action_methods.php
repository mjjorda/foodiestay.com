<?php

use \DrewM\MailChimp\MailChimp;

## methods post
## recibe action post in xeki
//$sql=$AG_MODULES->ag_module_import('ag_db_sql','main');
//require_once dirname(__FILE__) . '/common/core_methods.php';
//require_once dirname(__FILE__) . '/common/mail.php';
$vl_action = $AG_ACTION = $_POST['AG_ACTION'];
$vl_values = $_POST;

$sql = \xeki\module_manager::import_module("ag_db_sql");
$ag_auth = \xeki\module_manager::import_module("ag_auth");
if (false) {
} else if ($vl_action == 'contacto') {
    #agregar user al boletin
    $query = 'SELECT * FROM boletin_emails where email=' . $vl_values['email'];
    if (count($sql->query($query)) > 0) {
        $data = array(
            'nombre' => $vl_values['nombre'],
            'email' => $vl_values['email'],
            'date' => date("Y-m-d"),
        );
        $res = $sql->insert('boletin_emails', $data);
    }

    $data = array(
        'nombre' => limpiarCadena($vl_values['nombre']),
        'email' => limpiarCadena($vl_values['email']),
        'aunto' => limpiarCadena($vl_values['aunto']),
        'comentario' => limpiarCadena($vl_values['comentario']),
        'date' => date("Y-m-d"),
    );
    $res = $sql->insert('client_contacto', $data);
//    messageContactEmail($data['nombre'], $data['email'], $data['aunto'], $data['comentario']);
    ag_pushMsg("¡GRACIAS POR CONTACTARNOS! Hemos recibido correctamente tu mensaje, en breve nos comunicaremos contigo.");
} else if ($vl_action == 'reserve') {
//  $_POST;
//  d($_POST);

    $_SESSION['buy_process'] = array();
    $_SESSION['buy_process']['id_book'] = $_POST['id_book'];
    $_SESSION['buy_process']['type_book'] = $_POST['type_book'];
    $_SESSION['buy_process']['persons'] = $_POST['persons'];
    $_SESSION['buy_process']['apartments'] = $_POST['apartments'];



    $info_user = $ag_auth->get_user_info();
    // TODO create reserve update disponibility

    $id_user = $info_user['id'];
    $id_book = $_SESSION['buy_process']['id_book'];
    $res = array();
    if($ag_auth->check_auth()) {
        $query = "SELECT * FROM user_buy where user_ref='{$id_user}' and book_ref='$id_book'";
        $res = $sql->query($query);
    }

    if(count($res)>0){
        $_SESSION['buy_process']['reserve']=$res[0]['id'];
        $data = array(
            "user_ref"=>$id_user,
            "book_ref"=>$id_book,
            "number_persons"=>$_POST['persons'],
            "apartment_ref"=>$_POST['apartments'],
        );
        $sql->update("user_buy",$data,"id='{$res[0]['id']}'");

    }
    else{
        $data = array(
            "user_ref"=>$id_user,
            "book_ref"=>$id_book,
            "number_persons"=>$_POST['persons'],
            "apartment_ref"=>$_POST['apartments'],
        );
        $id = $sql->insert("user_buy",$data);
        $_SESSION['buy_process']['reserve']=$id;
    }


    \xeki\core::redirect("book-process");

} else if ($vl_action == 'check_out') {
    // save new info with adds

    $_SESSION['buy_process']['cancellation'] = $_POST['cancellation'];
    $_SESSION['buy_process']['apartment'] = $_POST['apartment'];
    $_SESSION['buy_process']['private'] = $_POST['private'];

    //

    // user info


    // check active reserve with this id book
    //


    // create pre-reserve

    \xeki\core::redirect("checkout");
} else if ($vl_action == 'cancel') {
    $_SESSION['buy_process'] = array();

    $popUp = \xeki\module_manager::import_module('ag_popup');
    $popUp->add_msg("Book canceled");
    \xeki\core::redirect("books");

} else if ($vl_action == 'subscribe_to_email') {
//  $_POST;
//  d($_POST);
    // send email

    // send to mailchimp


    $MailChimp = new MailChimp('80de09b3273c178bfe6dd020dfc8cefd-us17');
    $result = $MailChimp->post("lists/1c12f5a731/members", [
        'email_address' => $vl_values['email'],
        'status' => 'subscribed',
    ]);

    $path_file = dirname(__FILE__) . "/pages/mails/subcribe.html";
    $content = file_get_contents($path_file);

    $mail = \xeki\module_manager::import_module("ag_mail");
    $mail->send_email($vl_values['email'],"Subscribe",$content,array());

    // popup
    $popUp = \xeki\module_manager::import_module('ag_popup');
    $popUp->add_msg("Welcome to Foodiestay community, you just subscribed succesfully to our newsletter");

    // TODO send email
}else if ($vl_action == 'contact') {
//  $_POST;
//  d($_POST);
    // send email

    // send to mailchimp



    $path_file = dirname(__FILE__) . "/pages/mails/contact.html";
    $content = file_get_contents($path_file);


    $data = array(
        "name" => $vl_values['name'],
        "email" => $vl_values['email'],
        "phone" => $vl_values['phone'],
        "subject" => $vl_values['subject'],
        "message" => $vl_values['message'],
    );

    $mail = \xeki\module_manager::import_module("ag_mail");
    $mail->send_email($vl_values['email'],"Contact email",$content,$data);

    // popup
    $popUp = \xeki\module_manager::import_module('ag_popup');
    $popUp->add_msg("Thank you for getting in touch, we succesfully received your message");


    // TODO send email
}
else if ($vl_action == 'save_book') {
//  $_POST;
    // send email

    // send to mailchimp
    if ($ag_auth->check_auth()) {
        // if exist delete

        // if not exist
        $info = $ag_auth->get_user_info();

        $query = "SELECT * FROM user_saved_books where user_ref='{$info['id']}' and book_ref='{$vl_values['book_ref']}'";

        $res = $sql->query($query);
//        d($query);
//        d($res);
        if (count($res) > 0) {
            $res = $sql->delete("user_saved_books", " id = {$res[0]['id']}");
            \xeki\core::redirect("books/");
        } else {
            $data = array(
                "user_ref" => $info['id'],
                "book_ref" => $vl_values['book_ref'],
            );
            $sql->insert("user_saved_books", $data);
            \xeki\core::redirect("user-books-save");
        }


    }
    else{
        \xeki\core::redirect("register");

    }


}
elseif ($vl_action == 'udpate_user') {

    $info = $ag_auth->get_user_info();
    $data = array(
        "name"=>$vl_values["name"],
        "last_name"=>$vl_values["last_name"],
        "phone"=>$vl_values["phone"],
        "lang"=>$vl_values["lang"],
    );
    $sql->update("user",$data,"id='{$info['id']}'");
    $ag_auth->updateUserInfo();
    \xeki\core::redirect("account");
}


// ev2 zone methods
function ag_pushMsg($item)
{
    d($item);
}