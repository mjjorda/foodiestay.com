<?php
/**
 * Created by PhpStorm.
 * User: Liuspatt
 * Date: 3/4/2016
 * Time: 5:03 PM
 */

require_once(dirname(__FILE__) . "/models/foodie.php");

// Modules 
$sql = \xeki\module_manager::import_module('ag_db_sql', 'main');
$ag_config = \xeki\module_manager::import_module('ag_config');
$ag_auth = \xeki\module_manager::import_module("ag_auth");

// d($_SESSION['AG_CONFIG_LANG']);
// for handling lang and codes
if ($ag_auth->check_auth()) { // auth load from user
    $user_info = $ag_auth->get_user_info();
    $ag_config->set_active_lang($user_info['lang']);
} else { // load for location
    if (!isset($_SESSION['AG_CONFIG_LANG'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $data = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip={$ip}"));
        $country_code = $data['geoplugin_countryCode'];
        $lang = country_lang($country_code);
        // d($lang);
        // d($country_code);
        
        $ag_config->set_active_lang($lang);
    }
}

// data for i18n

//$config->load_local_lang();
$config_data=$ag_config->get_data();
// d($config_data);
$AG_HTML->add_extra_data("i18n", $config_data);

// $res = $sql->query("SELECT * FROM ag_catalog_category where active='Si' order by order_list asc");
// $AG_HTML->add_extra_data("categories_list",$res);

// places

//d("run main");
//d($_SESSION['task']['post_login']);
if ($ag_auth->check_auth()) {
    if (isset($_SESSION['task']['post_login'])) {
        $_SESSION['task'] = array();
        $info_user = $ag_auth->get_user_info();
        $data = array(
            "user_ref" => $info_user['id'],
        );
        $sql->update("user_buy", $data, "id='{$_SESSION['buy_process']['reserve']}'");
        \xeki\core::redirect("checkout");
    }


    $info = $ag_auth->get_user_info();
//    d($info);
    if ($info['activated'] == "off") {

        if ($info['idFacebook'] != "") {
            $data = array(
                "activated" => "on",
                "confirm_code" => "",
            );
            $sql->update("user", $data, "id='{$info['data']}'");
        } else {
            $last_param = \xeki\core::$URL_PARAMS[0];
            if ($last_param != "confirm-account") {
                \xeki\core::redirect("confirm-account");
            }
        }


    }

}
$query = "SELECT name,slug FROM places where active='on' order by order_list asc";
$places = $sql->query($query);


//d($places);
$AG_HTML->add_extra_data("places", $places);
$AG_HTML->add_extra_data("somedata", "This is spartaa"); // this info work aaas {{somedata}} in html pages


// List countries with lang
function country_lang($key)
{
    $items= array(
        "AD" => "ca", "AE" => "ar","AF" => "fa","AG" => "en","AI" => "en","AL" => "sq","AM" => "hy","AO" => "pt","AR" => "es","AS" => "en","AT" => "de","AU" => "en","AW" => "nl","AX" => "sv","AZ" => "az","BA" => "bs","BB" => "en","BD" => "bn","BE" => "nl","BF" => "fr","BG" => "bg","BH" => "ar","BI" => "fr","BJ" => "fr","BL" => "fr","BM" => "en","BN" => "ms","BO" => "es","BQ" => "nl","BR" => "pt","BS" => "en","BT" => "dz","BW" => "en","BY" => "be","BZ" => "en","CA" => "en","CC" => "ms","CD" => "fr","CF" => "fr","CG" => "fr","CH" => "de","CI" => "fr","CK" => "en","CL" => "es","CM" => "en","CN" => "zh","CO" => "es","CR" => "es","CU" => "es","CV" => "pt","CW" => "nl","CX" => "en","CY" => "el","CZ" => "cs","DE" => "de","DJ" => "fr","DK" => "da","DM" => "en","DO" => "es","DZ" => "ar","EC" => "es","EE" => "et","EG" => "ar","EH" => "ar","ER" => "aa","ES" => "es","ET" => "am","FI" => "fi","FJ" => "en","FK" => "en","FM" => "en","FO" => "fo","FR" => "fr","GA" => "fr","GB" => "en","GD" => "en","GE" => "ka","GF" => "fr","GG" => "en","GH" => "en","GI" => "en","GL" => "kl","GM" => "en","GN" => "fr","GP" => "fr","GQ" => "es","GR" => "el","GS" => "en","GT" => "es","GU" => "en","GW" => "pt","GY" => "en","HK" => "zh","HN" => "es","HR" => "hr","HT" => "ht","HU" => "hu","ID" => "id","IE" => "en","IL" => "he","IM" => "en","IN" => "en","IO" => "en","IQ" => "ar","IR" => "fa","IS" => "is","IT" => "it","JE" => "en","JM" => "en","JO" => "ar","JP" => "ja","KE" => "en","KG" => "ky","KH" => "km","KI" => "en","KM" => "ar","KN" => "en","KP" => "ko","KR" => "ko","XK" => "sq","KW" => "ar","KY" => "en","KZ" => "kk","LA" => "lo","LB" => "ar","LC" => "en","LI" => "de","LK" => "si","LR" => "en","LS" => "en","LT" => "lt","LU" => "lb","LV" => "lv","LY" => "ar","MA" => "ar","MC" => "fr","MD" => "ro","ME" => "sr","MF" => "fr","MG" => "fr","MH" => "mh","MK" => "mk","ML" => "fr","MM" => "my","MN" => "mn","MO" => "zh","MP" => "fil","MQ" => "fr","MR" => "ar","MS" => "en","MT" => "mt","MU" => "en","MV" => "dv","MW" => "ny","MX" => "es","MY" => "ms","MZ" => "pt","NA" => "en","NC" => "fr","NE" => "fr","NF" => "en","NG" => "en","NI" => "es","NL" => "nl","NO" => "no","NP" => "ne","NR" => "na","NU" => "niu","NZ" => "en","OM" => "ar","PA" => "es","PE" => "es","PF" => "fr","PG" => "en","PH" => "tl","PK" => "ur","PL" => "pl","PM" => "fr","PN" => "en","PR" => "en","PS" => "ar","PT" => "pt","PW" => "pau","PY" => "es","QA" => "ar","RE" => "fr","RO" => "ro","RS" => "sr","RU" => "ru","RW" => "rw","SA" => "ar","SB" => "en","SC" => "en","SD" => "ar","SS" => "en","SE" => "sv","SG" => "cmn","SH" => "en","SI" => "sl","SJ" => "no","SK" => "sk","SL" => "en","SM" => "it","SN" => "fr","SO" => "so","SR" => "nl","ST" => "pt","SV" => "es","SX" => "nl","SY" => "ar","SZ" => "en","TC" => "en","TD" => "fr","TF" => "fr","TG" => "fr","TH" => "th","TJ" => "tg","TK" => "tkl","TL" => "tet","TM" => "tk","TN" => "ar","TO" => "to","TR" => "tr","TT" => "en","TV" => "tvl","TW" => "zh","TZ" => "sw","UA" => "uk","UG" => "en","UM" => "en","US" => "en","UY" => "es","UZ" => "uz","VA" => "la","VC" => "en","VE" => "es","VG" => "en","VI" => "en","VN" => "vi","VU" => "bi","WF" => "wls","WS" => "sm","YE" => "ar","YT" => "fr","ZA" => "zu","ZM" => "en","ZW" => "en","CS" => "cu","AN" => "nl",);
    return isset($items[$key])?$items[$key]:"en";
}




