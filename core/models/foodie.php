<?php
class foodie
{
    // Configurable variables

    private $sql = null;
    private $ag_auth = null;

    private $logged_user = false;


    function __construct()
    {

        $this->ag_auth = \xeki\module_manager::import_module("ag_auth");
        $this->sql=\xeki\module_manager::import_module("ag_db_sql");

        if($this->ag_auth->check_auth()) {
            $this->logged_user = true;
        }
    }


    function get_books($limit=9){
        $books_return = array();
        if($this->logged_user){
            $info = $this->ag_auth->get_user_info();
            $query = "
                select * from 
                books left join user_saved_books on books.id=user_saved_books.book_ref and '{$info['id']}'=user_saved_books.user_ref
                where books.active = 'on'
                and books.visible_alone !='on'
                order by books.id desc
                limit $limit";

//            d($query);

            $res = $this->sql->query($query,true);
//            d($res);
            foreach ($res as $item){
                $temp_array=$item['books'];
                if($item['user_saved_books']['id']!=null && $item['user_saved_books']['id']!=''){
                    $temp_array['saved']=true;
                }
                array_push($books_return,$temp_array);
            }




        }
        else{
            $query = "
                SELECT * from books where books.active = 'on'
                and books.visible_alone !='on'
                order by id desc limit $limit ";

            $books_return = $this->sql->query($query);
        }
//        d($books_return);

        $query = "SELECT * from places where active = 'on'";
        $places = $this->sql->query($query);
        $places_id = array();
        foreach ($places as $key => $item){
            $places_id[$item['id']]=$item;
        }


        foreach ($books_return as $key => $item){
            $books_return[$key]['country_name'] = $places_id[$item['place']]['name'];

            if($item['date_end']=='0000-00-00'){
                $books_return[$key]['n_days']= 1;
            }
            else{
                $now = time(); // or your date as well
                $date_ini = strtotime($item['date_ini']);
                $date_end = strtotime($item['date_end']);
                $datediff = $date_end - $date_ini;

                $books_return[$key]['n_days'] = round($datediff / (60 * 60 * 24));
            }

            // hours
            $hour_ini = $item['hour_ini'];
            $hour_ini = explode(":",$hour_ini)[0];
            $hour_end = $item['hour_end'];
            $hour_end = explode(":",$hour_end)[0];

            $hours = $item['hour_end']-$item['hour_ini'];
            $books_return[$key]['hours']= $hours;
            // calculate days
//    d($item);
        }

        return $books_return;
    }
}