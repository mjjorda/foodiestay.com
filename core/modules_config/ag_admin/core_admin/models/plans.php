<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Plans";
$single_name = "Plan";
$table = "plans"; # for db ( maybe multiple data bases for ref)
$code = "plans"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</div>
    <div id_form="form_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-bed" aria-hidden="true"></i> Apartment</div>
    <!--<div id_form="form_actions_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-cogs" aria-hidden="true"></i> Acciones</div>-->
    <!--<div id_form="form_edit_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>
HTML;


$model_form = array(

    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Name",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"description", #name db field
        "title"=>"Description",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Dates",
    ),

    array(
        "type"=>"date",
        "name"=>"date_ini", #name db field
        "title"=>"Initial date",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"date",
        "name"=>"date_end", #name db field
        "title"=>"End date",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),



    array(
        "type"=>"bool",
        "name"=>"all_time", #name db field
        "title"=>"All time",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"separator",
        "title"=>"General",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"general_text", #name db field
        "title"=>"General",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"image",
        "name"=>"general_image", #name db field
        "title"=>"Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"image",
        "name"=>"general_video", #name db field
        "title"=>"Video",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"separator",
        "title"=>"Included",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_txt", #name db field
        "title"=>"Incluyed",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_not_txt", #name db field
        "title"=>"Incluyed not",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Options ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"options_txt", #name db field
        "title"=>"Options",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Important ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"important_txt", #name db field
        "title"=>"Important",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Optional ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"optional_txt", #name db field
        "title"=>"Optional",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"General ",
    ),

    array(
        "type"=>"image",
        "name"=>"banner_image", #name db field
        "title"=>"Banner Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"image",
        "name"=>"list_image", #name db field
        "title"=>"List Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),


    array(
        "type"=>"separator",
        "title"=>"Info Large",
    ),


    array(
        "type"=>"separator",
        "title"=>"Config",
    ),


    array(
        "type"=>"text",
        "name"=>"slug", #name db field
        "title"=>"Slug ***",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),


    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-plans") {
    $element_table_plans = array(
        "type" => "table",
        "text" => "Plans",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "plans", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Name",
                ),
//                array(
//                    "title" => "Language",
//                ),
//                array(
//                    "title" => "Currency",
//                ),
//                array(
//                    "title" => "Country",
//                ),
                array(
                    "title" => "Active",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_plans);
}

if ($module_action_code == "ws_plans") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
//    array_push($columns, array("db" => "lang", "dt" => count($columns)));
//    array_push($columns, array("db" => "currency", "dt" => count($columns)));
//    array_push($columns, array("db" => "country", "dt" => count($columns)));
    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_plans") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Plan</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_plans" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM plans where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Plan</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_actions_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Actions Plan</h2>
            <form method="post">
              <input name="ag_admin_action" value="send_change_pass_plans" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Send Email Recover Pass</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Plan</h2>
                <hr>
              <input name="ag_admin_action" value="delete_plans" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_plans"){


    $data=array(
        "name"=>$values["name"],
        "description"=>$values["description"],
        "date_ini"=>$values["date_ini"],
        "date_end"=>$values["date_end"],
        "all_time"=>$values["all_time"],
        "general_text"=>$values["general_text"],
        "general_image"=>$values["general_image"],
        "general_video"=>$values["general_video"],
        "incluyed_txt"=>$values["incluyed_txt"],
        "incluyed_not_txt"=>$values["incluyed_not_txt"],
        "options_txt"=>$values["options_txt"],
        "important_txt"=>$values["important_txt"],
        "optional_txt"=>$values["optional_txt"],
        "slug"=>$values["slug"],
        "active"=>$values["active"],
    );

    // add images
    $images_files=array(
        'general_image' => 'none',
        'banner_image' => 'none',
    );
    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;
    $res = $ag_sql->insert("plans",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("plans",{$res});
JS;
    }
}

if($values["ag_admin_action"]=="edit_plans"){

    $data=array(
        "name"=>$values["name"],
        "description"=>$values["description"],
        "date_ini"=>$values["date_ini"],
        "date_end"=>$values["date_end"],
        "all_time"=>$values["all_time"],
        "general_text"=>$values["general_text"],
        "general_image"=>$values["general_image"],
        "general_video"=>$values["general_video"],
        "incluyed_txt"=>$values["incluyed_txt"],
        "incluyed_not_txt"=>$values["incluyed_not_txt"],
        "options_txt"=>$values["options_txt"],
        "important_txt"=>$values["important_txt"],
        "optional_txt"=>$values["optional_txt"],
        "slug"=>$values["slug"],
        "active"=>$values["active"],
    );

    // add images
    $images_files=array(
        'general_image' => 'none',
        'banner_image' => 'none',
    );

    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $array_json['processed_images_']=$images_files;
    $array_json['processed_images__']=$_FILES;

    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;

    $res = $ag_sql->update("plans",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("plans",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_plans"){
    $render_method = "json";
    $res = $ag_sql->delete("plans"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}