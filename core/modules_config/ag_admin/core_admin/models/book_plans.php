<?php
// view select books.id,books.name,places.name "country",cities.name "city",books.date_ini,books.date_end,books.price,books.active from books,cities,places
//where
//books.city=cities.id and
//books.place=places.id
// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Programas";
$single_name = "Programa";
$table = "books"; # for db ( maybe multiple data bases for ref)
$table_view = "books_view_plans";
$code_type = "plans";
/* view query

SELECT
        `books`.`id` AS `id`,
        `books`.`name` AS `name`,
        `places`.`name` AS `place_name`,
        `cities`.`name` AS `city_name`,
        `books`.`price` AS `price`,
        `books`.`date_ini` AS `date_ini`,
        `books`.`date_end` AS `date_end`,
        `books`.`active` AS `active`
    FROM
        ((`books`
        JOIN `cities`)
        JOIN `places`)
    WHERE
        ((`books`.`city` = `cities`.`id`)
            AND (`books`.`place` = `places`.`id`)
            AND (`books`.`type_book` = 'plans'))
    ORDER BY `books`.`id` DESC
*/
$code = "plans"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> 1. Datos b&aacute;sicos</div>
    <div id_form="form_images_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-image" aria-hidden="true"></i> 2. Im&aacute;genes</div>
    
    <div id_form="form_dates_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-clock-o" aria-hidden="true"></i> 3. Duraci&oacute;n</div>
    <div id_form="form_edit_days_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-clock-o" aria-hidden="true"></i> 4. D&iacute;as</div>
    <div id_form="form_apartments_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-bed" aria-hidden="true"></i> 5. Alojamiento</div>
    <!--<div id_form="form_edit_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div class="open_sub_items "><i class="fa fa-address-book" aria-hidden="true"></i> 6. Detalles
        <div class="sub_items"> 
            <div id_form="form_info_general_plans" id_item="{$id_item}" class="admin-btn"><i class="fa " aria-hidden="true"></i> General </div>
            <div id_form="form_info_included_plans" id_item="{$id_item}" class="admin-btn"><i class="fa " aria-hidden="true"></i> Included </div>
            <div id_form="form_info_options_plans" id_item="{$id_item}" class="admin-btn"><i class="fa " aria-hidden="true"></i> Options </div>
            <div id_form="form_info_important_plans" id_item="{$id_item}" class="admin-btn"><i class="fa " aria-hidden="true"></i> Important </div>
            <div id_form="form_info_optional_plans" id_item="{$id_item}" class="admin-btn"><i class="fa " aria-hidden="true"></i> Optional </div>
        </div>
    </div>
    <div id_form="form_delete_plans" id_item="{$id_item}" class="admin-btn"><i class="fa fa-exclamation" aria-hidden="true"></i> 7. Eliminar</div>
HTML;



$model_dates =  array(
//    array(
//        "type"=>"select",
//        "select_options" =>array(
//            "always" => "Secuencia de d&iacute;as",
//            "temporary" => "Llegada y salida",
//        ),
//        "name"=>"type_times", #name db field
//        "title"=>"Tipo de book",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"",
//    ),
    array(
        "type"=>"date",
        "name"=>"date_ini", #name db field
        "title"=>"Fecha llegada",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"date",
        "name"=>"date_end", #name db field
        "title"=>"Fecha salida:",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
//    array(
//        "type"=>"date_time",
//        "name"=>"hour_ini", #name db field
//        "title"=>"Hora llegada",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),
//    array(
//        "type"=>"date_time",
//        "name"=>"hour_end", #name db field
//        "title"=>"Hora Salida",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),
//    array(
//        "type"=>"bool",
//        "name"=>"visible_alone", #name db field
//        "title"=>"¿Visible s&oacute;lo para programas?",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),

);
$model_dates_days =  array(
    array(
        "type"=>"bool",
        "name"=>"available_monday", #name db field
        "title"=>"Lunes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_tuesday", #name db field
        "title"=>"Martes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_wednesday", #name db field
        "title"=>"Miercoles",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_thursday", #name db field
        "title"=>"Jueves",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_friday", #name db field
        "title"=>"Viernes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_saturday", #name db field
        "title"=>"Sabado",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_sunday", #name db field
        "title"=>"Domingo",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
);


$model_form_create =  array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Titulo de experiencia",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"description", #name db field
        "title"=>"Descripci&oacute;n corta",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"place", #name db field
        "title"=>"Pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"cities",
        "table_title" =>"name",
        "name"=>"city", #name db field
        "title"=>"Ciudad",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"text",
        "name"=>"location", #name db field
        "title"=>"Ubicaci&oacute;n",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"textarea",
        "name"=>"tags", #name db field
        "title"=>"Tags",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"number",
        "name"=>"limit_persons", #name db field
        "title"=>"Limite personas",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"price", #name db field
        "title"=>"Precio total ( eur )",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"base_price_persons", #name db field
        "title"=>"Cantidad personas incluidas en precio:",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"add_person_price", #name db field
        "title"=>"Precio persona adicional (eur)",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),




    array(
        "type"=>"bool",
        "name"=>"option_secure", #name db field
        "title"=>"Con opci&oacute;n de seguro de cancelaci&oacute;n",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"number",
        "name"=>"cancellation_person_price", #name db field
        "title"=>"Seguro cancelacion por persona (eur)",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"bool",
        "name"=>"option_private_experience", #name db field
        "title"=>"Con opci&oacute;n de experiencia privada",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"number",
        "name"=>"private_price", #name db field
        "title"=>"Precio privado",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

//    array(
//        "type"=>"select",
//        "select_options" =>array(
//            "always" => "Secuencia de d&iacute;as",
//            "temporary" => "Llegada y salida",
//        ),
//        "name"=>"type_times", #name db field
//        "title"=>"Tipo de book",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"",
//    ),
//    array(
//        "type"=>"bool",
//        "name"=>"visible_alone", #name db field
//        "title"=>"Visible s&oacute;lo para programas?",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),
    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);

$model_form_images =  array(
    array(
        "type"=>"image",
        "name"=>"banner_image", #name db field
        "title"=>"Imagen cabezote principal",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"image",
        "name"=>"list_image", #name db field
        "title"=>"Imagen previsualizaci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"image",
        "name"=>"big_format_image", #name db field
        "title"=>"Imagen gran formato",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),
);

$general_form = array(
    array(
        "type"=>"admin_blog",
        "name"=>"general_text", #name db field
        "title"=>"General",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"image",
        "name"=>"general_image", #name db field
        "title"=>"Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"video",
        "name"=>"general_video", #name db field
        "title"=>"Video",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),
);

$included_form = array(
    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_txt", #name db field
        "title"=>"Incluyed",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_not_txt", #name db field
        "title"=>"Incluyed not",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
);

$options_form = array(
    array(
        "type"=>"admin_blog",
        "name"=>"options_txt", #name db field
        "title"=>"Options",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
);
$important_form = array(
    array(
        "type"=>"admin_blog",
        "name"=>"important_txt", #name db field
        "title"=>"Important",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
);

$optional_form = array(
    array(
        "type"=>"admin_blog",
        "name"=>"optional_txt", #name db field
        "title"=>"Optional",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
);
// full form
$model_form = array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Titulo de experiencia:",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"price", #name db field
        "title"=>"Precio ( eur )",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"place", #name db field
        "title"=>"Pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select",
        "select_options" =>array(
            "always" => "Fija",
            "temporary" => "Temporal",
        ),
        "name"=>"type_times", #name db field
        "title"=>"Tipo de book",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"description", #name db field
        "title"=>"Descripci&oacute;n Corta",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

//    array(
//        "type"=>"separator",
//        "title"=>"Fechas",
//    ),
//
//    array(
//        "type"=>"date",
//        "name"=>"date_ini", #name db field
//        "title"=>"Fecha",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),
//    array(
//        "type"=>"date",
//        "name"=>"date_end", #name db field
//        "title"=>"End date",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),



//    array(
//        "type"=>"bool",
//        "name"=>"all_time", #name db field
//        "title"=>"All time",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),

    array(
        "type"=>"separator",
        "title"=>"General",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"general_text", #name db field
        "title"=>"General",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"image",
        "name"=>"general_image", #name db field
        "title"=>"Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"video",
        "name"=>"general_video", #name db field
        "title"=>"Video",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"separator",
        "title"=>"Included",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_txt", #name db field
        "title"=>"Incluyed",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_not_txt", #name db field
        "title"=>"Incluyed not",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Options ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"options_txt", #name db field
        "title"=>"Options",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Important ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"important_txt", #name db field
        "title"=>"Important",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Optional ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"optional_txt", #name db field
        "title"=>"Optional",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"General ",
    ),

    array(
        "type"=>"image",
        "name"=>"banner_image", #name db field
        "title"=>"Banner Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"image",
        "name"=>"list_image", #name db field
        "title"=>"List Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),


//    array(
//        "type"=>"separator",
//        "title"=>"Info Large",
//    ),
//
//
    array(
        "type"=>"separator",
        "title"=>"Config",
    ),


//    array(
//        "type"=>"text",
//        "name"=>"slug", #name db field
//        "title"=>"Slug ***",
//        "required"=>"",
//        "value"=>"",
//        "description"=>"",
//    ),
    array(
        "type"=>"bool",
        "name"=>"visible_alone", #name db field
        "title"=>"Visible individualmente",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-plans") {
    $element_table_plans = array(
        "type" => "table",
        "text" => "Programas",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "plans", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Id",
                ),
                array(
                    "title" => "Nombre",
                ),
                array(
                    "title" => "País",
                ),
                array(
                    "title" => "Ciudad",
                ),
                array(
                    "title" => "Fecha llegada",
                ),
                array(
                    "title" => "Fecha salida",
                ),
                array(
                    "title" => "Precio",
                ),
//                array(
//                    "title" => "Language",
//                ),
//                array(
//                    "title" => "Currency",
//                ),
//                array(
//                    "title" => "Country",
//                ),
                array(
                    "title" => "Visible",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_plans);
}

if ($module_action_code == "ws_plans") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table_view}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
    array_push($columns, array("db" => "place_name", "dt" => count($columns)));
    array_push($columns, array("db" => "city_name", "dt" => count($columns)));
    array_push($columns, array("db" => "date_ini", "dt" => count($columns)));
    array_push($columns, array("db" => "date_end", "dt" => count($columns)));
//    array_push($columns, array("db" => "lang", "dt" => count($columns)));
//    array_push($columns, array("db" => "currency", "dt" => count($columns)));
//    array_push($columns, array("db" => "country", "dt" => count($columns)));
    array_push($columns, array("db" => "price", "dt" => count($columns)));
    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_plans") {

    $field_controls="";
    foreach($model_form_create as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>Nueva Programa</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_plans" type="hidden">
  <button type="submit" class="btn btn-primary">Guardar y continuar</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_apartments_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];

    // get categories
    $query = "select * from apartments ba
left join apartments_ref bac
	on bac.id_apartment=ba.id and bac.id_book= {$id_item};";
    $res_query = $sql->query($query,true);
    $selected = array();
    $no_selected = array();
    foreach ($res_query as $item) {
        if ($item['bac']['id_book']!== null)
            array_push($selected, $item);
        else
            array_push($no_selected, $item);
    }
//    die();
    $html_selected = <<<HTML
       
HTML;
    // generate html
    foreach ($selected as $item) {
        $html_selected .= <<<HTML
        
         <form method="post">
            <input name="ag_admin_action" value="remove_book_apartment_plans" type="hidden">
            <input name="id_item" value="{$id_item}"  type="hidden">
            <input name="id" value="{$item["bac"]["id"]}" type="hidden">
            <button type="submit" class="btn btn-success"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{$item['ba']["name"]}</button>
         </form>
HTML;
    }

    $html_no_selected = <<<HTML
        
HTML;
    foreach ($no_selected as $item) {
        $html_no_selected .= <<<HTML
        <form method="post">
          <input name="ag_admin_action" value="add_book_apartment_plans" type="hidden">
          <input name="id_item" value="$id_item"  type="hidden">
          <input name="id_ref"  value="{$item['ba']['id']}" type="hidden">
          <button type="submit" class="btn btn-add-item"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{$item['ba']["name"]}</button>
        </form>
HTML;
    }

    // list categories selected

    // lista categories unselected
    $html = <<< HTML
    <div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <h2>Hospedajes</h2>
        <p>Hospedajes disponibles para este el book</p>
        <hr>
        
        <h4>Hospedajes seleccionados</h4>
        <p>Estos son los hospedajes que apareceran en el book </p>
        {$html_selected}
        <hr>
        <h4>Hospedajes para este book</h4>
        <p>Estos son los hospedajes disponibles para activar en este book  </p>
        {$html_no_selected}
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}

if($values["ag_admin_action"]=="add_book_apartment_plans"){
    $render_method = "json";
    $data=array(
        "id_book"=>$values["id_item"],
        "id_apartment"=>$values["id_ref"],

    );
    $res = $ag_sql->insert("apartments_ref",$data);
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['data']=$data;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_apartments_plans",{$values['id_item']});
JS;
    }
}

if($values["ag_admin_action"]=="remove_book_apartment_plans"){
    $render_method = "json";
    $res = $ag_sql->delete("apartments_ref"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_apartments_plans",{$values['id_item']});
JS;
    }
}


// actions




if ($module_action_code == "form_edit_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form_create as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Programa</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_dates_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];

    foreach($model_dates as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    if($info['type_times']=="always"){
        $field_controls.="De la fecha {$info['date_ini']} a {$info['date_end']} se repitir&aacutel el book los d&iacute;as seleccionados.";
        foreach($model_dates_days as $item){
            $item['value']=$info[$item['name']];
            $html_form = $ag_admin_module->form_generator($item);
            $field_controls.=$html_form;
        }
    }


    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Fechas y horarios experiencia</h2>
            <hr>
            <form method="post">
            {$field_controls}
            
              <input name="ag_admin_action" value="edit_dates_plans" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}


if ($module_action_code == "form_edit_days_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
    $json_inners_items = $info['json_inners_items'];
    $json_inners_items = json_decode($json_inners_items,true);
    // count days
    $date_ini=$info['date_ini'];
    $date_end=$info['date_end'];
    $end_date = strtotime($date_end);
    $ini_date = strtotime($date_ini);
    $datediff = $end_date - $ini_date;

    $days = round($datediff / (60 * 60 * 24));
    $days++;

    $days_form = "";
    for ($i=0;$i<$days;$i++){
        $day_n = $i+1;
        // get books
        $date_day= date("Y-m-d",strtotime($date_ini)+((60 * 60 * 24)*$i));
        $query = "select * from books where date_ini=date_ini and date_ini='$date_day' and (type_book='exp' or type_book='foo') and active='on'";
//        $query = "select * from books where (type_book='exp' or type_book='foo') and active='on'";
        $info_options = $sql->query($query);
        $options_html ="";

        $html_options="<option value=''>Seleccionar</option>";

        foreach ($info_options as $key=>$value_local){
            $selected = $json_inners_items[$day_n];
            $key = $value_local['id'];
            $select = $key==$selected?"selected":"";
            $html_options.="<option value='{$key}' $select>{$value_local['name']}</option>";
        }

        $days_form.=<<<HTML
            <div class="form-group">
                <label for="id_day_{$day_n}"><b>D&iacute;a {$day_n} </b><small>{$date_day}</small></label>
                <select name="plans_day[{$day_n}]" class="form-control" required>
                  {$html_options}
                </select>
            </div>
            <div class=" form-group">                
                <small class="form-text text-muted"></small>
            </div>
HTML;

    }


    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Days</h2>
            <p>Selecciona el fooday o experiencia que va en cada d&iacute;a. Solo salen los foodays y las experiencias de un dia activas para cada d&iacute;a. </p>
            <hr>
            <form method="post">
                <div class=" form-group">
                   <label>
                   {$days} d&iacute;as <small>{$date_ini} - {$date_end}</small>
                   </label>
               </div>
               
               {$days_form}
                
            
              <input name="ag_admin_action" value="edit_days_plans" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}

if ($module_action_code == "form_delete_plans") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Programa</h2>
                <hr>
              <input name="ag_admin_action" value="delete_plans" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_plans"){


    $data = $ag_admin_module->process_data($model_form_create,$values);

    $array_json['data']=$data;
    $data['type_book']=$code_type;
    $res = $ag_sql->insert("{$table}",$data);
    $array_json['error']=$ag_sql->error();

    $slug = fix_to_slug("{$values["name"]}_e_{$res}");
    $data=array(
        "slug"=>$slug
    );
    $ag_sql->update("{$table}",$data,"id='{$res}'");

    if(!$res){
//        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_images_plans",{$res});
JS;
    }
}

if($values["ag_admin_action"]=="edit_dates_plans"){
    $data = $ag_admin_module->process_data($model_dates,$values);
    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['data']=$data;
        $array_json['callback']= <<<JS
        js_admin.edit_item("plans",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="edit_days_plans"){


    $data=array(
        "json_inners_items"=>json_encode($values['plans_day']),
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else {
        $array_json['id_item']=$res;
        $array_json['data']=$data;
        $array_json['values']=$values;
        $array_json['callback'] = <<<JS
        js_admin.edit_item("days_plans",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="edit_plans"){

    $data = $ag_admin_module->process_data($model_form_create,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("plans",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_plans"){
    $render_method = "json";
    $res = $ag_sql->delete("{$table}"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}

/// block sub edit
///
///
///
///





if ($module_action_code == "form_images_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form_images as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Imagenes Programa</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_images_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_images_plans"){

    // add images
    $data = $ag_admin_module->process_data($model_form_images,$values);
    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_images_plans",{$values['id']});
JS;
    }
}





if ($module_action_code == "form_info_general_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($general_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>General</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_info_general_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_info_general_plans"){

    $data = $ag_admin_module->process_data($general_form,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_info_general_plans",{$values['id']});
JS;
    }
}

if ($module_action_code == "form_info_included_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($included_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Included</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_info_included_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_info_included_plans"){

    $data = $ag_admin_module->process_data($included_form,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_info_included_plans",{$values['id']});
JS;
    }
}



if ($module_action_code == "form_info_options_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($options_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Options</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_info_options_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_info_options_plans"){

    $data = $ag_admin_module->process_data($options_form,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_info_options_plans",{$values['id']});
JS;
    }
}


if ($module_action_code == "form_info_important_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($important_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Important</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_info_important_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_info_important_plans"){

    $data = $ag_admin_module->process_data($important_form,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_info_important_plans",{$values['id']});
JS;
    }
}


if ($module_action_code == "form_info_optional_plans") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($optional_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Optional</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_info_optional_plans" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Guardar y continuar</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if($values["ag_admin_action"]=="edit_info_optional_plans"){

    $data = $ag_admin_module->process_data($optional_form,$values);

    $array_json['data']=$data;

    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_info_optional_plans",{$values['id']});
JS;
    }
}
