<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Directorio Hospedaje";
$single_name = "Directorio Hospedaje";
$table = "apartments"; # for db ( maybe multiple data bases for ref)
$table_view = "apartments_view";
// view query
/*
select
apartments.id as id,
apartments.name as name,
places.name as place_name,
cities.name as city_name,
apartments.price as price,
apartments.capacity as capacity,
apartments.active as active
from apartments, cities,places
where apartments.city= cities.id and apartments.place= places.id
order by apartments.id desc;
*/
$code = "apartments"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</div>
    <!--<div id_form="form_actions_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-cogs" aria-hidden="true"></i> Acciones</div>-->
    <!--<div id_form="form_edit_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Borrar </div>
HTML;


$model_form = array(

    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"price", #name db field
        "title"=>"Precio ( eur )",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"number",
        "name"=>"capacity", #name db field
        "title"=>"# de huespedes",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"text",
        "name"=>"address", #name db field
        "title"=>"Direcci&oacute;n",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"place", #name db field
        "title"=>"Pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"cities",
        "table_title" =>"name",
        "name"=>"city", #name db field
        "title"=>"Ciudad",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"text",
        "name"=>"mini_info", #name db field
        "title"=>"Mini descripci&oacute;n",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"array_json",
        "array_json_data"=>array(
            array(
                "type"=>"image",
                "title"=>"Image",
                "value_name"=>"image",
                "preview"=>true,
            ),
        ),
        "name"=>"images_array", #name db field
        "title"=>"Im&aacute;genes",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
//    array(
//        "type"=>"image",
//        "name"=>"banner_image", #name db field
//        "title"=>"Image",
//        "required"=>"",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),

//    array(
//        "type"=>"separator",
//        "title"=>"Fechas",
//    ),
//
//    array(
//        "type"=>"date",
//        "name"=>"date_ini", #name db field
//        "title"=>"Fecha",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),
//    array(
//        "type"=>"date",
//        "name"=>"date_end", #name db field
//        "title"=>"End date",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),



//    array(
//        "type"=>"bool",
//        "name"=>"all_time", #name db field
//        "title"=>"All time",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),




//    array(
//        "type"=>"text",
//        "name"=>"slug", #name db field
//        "title"=>"Slug ***",
//        "required"=>"",
//        "value"=>"",
//        "description"=>"",
//    ),


    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-apartments") {
    $element_table_apartments = array(
        "type" => "table",
        "text" => "Apartamentos",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "apartments", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Id",
                ),
                array(
                    "title" => "Nombre",
                ),
                array(
                    "title" => "Ciudad",
                ),
                array(
                    "title" => "País",
                ),
                array(
                    "title" => "Precio",
                ),
                array(
                    "title" => "Total Huespedes",
                ),
                array(
                    "title" => "Visible",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_apartments);
}

if ($module_action_code == "ws_apartments") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table_view}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
    array_push($columns, array("db" => "place_name", "dt" => count($columns)));
    array_push($columns, array("db" => "city_name", "dt" => count($columns)));
    array_push($columns, array("db" => "price", "dt" => count($columns)));
    array_push($columns, array("db" => "capacity", "dt" => count($columns)));

    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_apartments") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>Nueva Apartamento</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_apartments" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_apartments") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Apartamento</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_apartments" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_actions_apartments") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Actions Apartamento</h2>
            <form method="post">
              <input name="ag_admin_action" value="send_change_pass_apartments" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Send Email Recover Pass</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_apartments") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Apartamento</h2>
                <hr>
              <input name="ag_admin_action" value="delete_apartments" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_apartments"){

    $data = $ag_admin_module->process_data($model_form,$values);
    $array_json['data']=$data;
    $res = $ag_sql->insert("{$table}",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("apartments",{$res});
JS;
    }
}

if($values["ag_admin_action"]=="edit_apartments"){

    $data = $ag_admin_module->process_data($model_form,$values);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("apartments",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_apartments"){
    $render_method = "json";
    $res = $ag_sql->delete("apartments"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}