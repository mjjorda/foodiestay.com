<?php
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Clients";
$single_name = "Company";
$table = "user"; # for db ( maybe multiple data bases for ref)
$code = "user"; # for urls


$html_inners_edit = <<<HTML
    

HTML;


$model_form = array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"last_name", #name db field
        "title"=>"Apellido",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"n_id", #name db field
        "title"=>"Cedula",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"email", #name db field
        "title"=>"Email",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    )


);

if ($module_action_code == "list-clients") {
    $element_table_client = array(
        "type" => "table",
        "text" => "Clients",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "client", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                "id"=>array(
                    "title" => "id",
                ),
                "name"=>array(
                    "title" => "Nombre",
                ),
                "last_name"=>array(
                    "title" => "Apellido",
                ),
                "n_id"=>array(
                    "title" => "Cédula",
                ),
                "Email"=>array(
                    "title" => "Verificado",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_client);
}

if ($module_action_code == "ws_client") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
    array_push($columns, array("db" => "last_name", "dt" => count($columns)));
    array_push($columns, array("db" => "n_id", "dt" => count($columns)));
    array_push($columns, array("db" => "activated", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_client") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Company</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_company" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_client") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $query = "SELECT * FROM user where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        <div id_form="form_edit_client" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Info</div>
        <div id_form="form_delete_client" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Edit Client</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_client" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if ($module_action_code == "form_delete_client") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            <div id_form="form_edit_client" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Info</div>
            <div id_form="form_delete_client" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Web String</h2>
                <hr>
              <input name="ag_admin_action" value="delete_client" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_client"){


    $data=array(
        "title"=>$values["title"],
        "nit"=>$values["nit"],
        "address"=>$values["address"],
        "phone_contact"=>$values["phone_contact"],
        "mail_contact"=>$values["mail_contact"],
    );

    // add images
    $array_json['data']=$data;
    $res = $ag_sql->insert("user",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("client",{$res});
JS;


    }



}
if($values["ag_admin_action"]=="edit_client"){


    $data=array(
        "name"=>$values['name'],
        "last_name"=>$values['last_name'],
        "n_id"=>$values['n_id'],
        "email"=>$values['email'],
    );

    $res = $ag_sql->update("user",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("client",{$values['id']});
JS;
    }
}


if($values["ag_admin_action"]=="delete_client"){
    $render_method = "json";
    $res = $ag_sql->delete("user"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}