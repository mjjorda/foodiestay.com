<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Feedback";
$single_name = "Feedback";
$table = "feedback"; # for db ( maybe multiple data bases for ref)
$code = "feedback"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_feedback" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</div>
    <!--<div id_form="form_actions_feedback" id_item="{$id_item}" class="admin-btn"><i class="fa fa-cogs" aria-hidden="true"></i> Acciones</div>-->
    <!--<div id_form="form_edit_feedback" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_feedback" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>    

HTML;


$model_form = array(
    array(
        "type"=>"text",
        "name"=>"user_name", #name db field
        "title"=>"User Name",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"image",
        "name"=>"user_image", #name db field
        "title"=>"User Image",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"stars", #name db field
        "title"=>"Stars ( 1 - 5 ) ",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"experience_name", #name db field
        "title"=>"Experience Name",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"feedback_text", #name db field
        "title"=>"Feedback",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Published",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-feedback") {
    $element_table_feedback = array(
        "type" => "table",
        "text" => "Feedbacks",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "feedback", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Date",
                ),
                array(
                    "title" => "User Name",
                ),
                array(
                    "title" => "Experience Name",
                ),
                array(
                    "title" => "Stars",
                ),
                array(
                    "title" => "Active",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_feedback);
}

if ($module_action_code == "ws_feedback") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "date_creation", "dt" => count($columns)));
    array_push($columns, array("db" => "user_name", "dt" => count($columns)));
    array_push($columns, array("db" => "experience_name", "dt" => count($columns)));
    array_push($columns, array("db" => "stars", "dt" => count($columns)));
    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_feedback") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Feedback</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_feedback" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_feedback") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM feedback where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Feedback</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_feedback" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_actions_feedback") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Actions Feedback</h2>
            <form method="post">
              <input name="ag_admin_action" value="send_change_pass_feedback" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Send Email Recover Pass</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_feedback") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Feedback</h2>
                <hr>
              <input name="ag_admin_action" value="delete_feedback" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_feedback"){


    $data=array(
        "user_name"=>$values["user_name"],
        "experience_name"=>$values["experience_name"],
        "feedback_text"=>$values["feedback_text"],
        "active"=>$values["active"],
        "stars"=>$values["stars"],
    );

    // add images
    $images_files=array(
        'user_image' => 'none',
    );
    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;
    $res = $ag_sql->insert("feedback",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("feedback",{$res});
JS;


    }



}
if($values["ag_admin_action"]=="edit_feedback"){


    $data=array(
        "user_name"=>$values["user_name"],
        "experience_name"=>$values["experience_name"],
        "feedback_text"=>$values["feedback_text"],
        "active"=>$values["active"],
        "stars"=>$values["stars"],
    );

    // add images
    $images_files=array(
        'user_image' => 'none',
    );
    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $array_json['processed_images_']=$images_files;
    $array_json['processed_images__']=$_FILES;

    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;

    $res = $ag_sql->update("feedback",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("feedback",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_feedback"){
    $render_method = "json";
    $res = $ag_sql->delete("feedback"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}