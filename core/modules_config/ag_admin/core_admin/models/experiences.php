<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Experiencias";
$single_name = "Experiencia";
$table = "books_experiences"; # for db ( maybe multiple data bases for ref)
$code = "experiences"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_experiences" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Datos Basicos</div>
    <div id_form="form_dates_experiences" id_item="{$id_item}" class="admin-btn"><i class="fa fa-clock-o" aria-hidden="true"></i> Fechas y horarios</div>
    <div id_form="form_apartments" id_item="{$id_item}" class="admin-btn"><i class="fa fa-bed" aria-hidden="true"></i> Hospedajes</div>
    <!--<div id_form="form_edit_experiences" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_experiences" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Borrar </div>
HTML;
$model_dates =  array(
    array(
        "type"=>"date",
        "name"=>"date_ini", #name db field
        "title"=>"Fecha inicio",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"date",
        "name"=>"date_ini", #name db field
        "title"=>"Fecha fin",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"date_time",
        "name"=>"hour_ini", #name db field
        "title"=>"Hora inicio",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"date_time",
        "name"=>"hour_end", #name db field
        "title"=>"Hora fin",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

);
$model_dates_days =  array(
    array(
        "type"=>"bool",
        "name"=>"available_monday", #name db field
        "title"=>"Lunes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_tuesday", #name db field
        "title"=>"Martes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_wednesday", #name db field
        "title"=>"Miercoles",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_thursday", #name db field
        "title"=>"Jueves",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_friday", #name db field
        "title"=>"Viernes",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_saturday", #name db field
        "title"=>"Sabado",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
    array(
        "type"=>"bool",
        "name"=>"available_sunday", #name db field
        "title"=>"Domingo",
        "required"=>"true",
        "value"=>"",
        "description"=>"Lunes",
    ),
);
$model_form_create =  array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"price", #name db field
        "title"=>"Precio ( eur )",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"place", #name db field
        "title"=>"Place",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select",
        "select_options" =>array(
            "always" => "Fija",
            "temporary" => "Temporal",
        ),
        "name"=>"type_times", #name db field
        "title"=>"Tipo de book",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"bool",
        "name"=>"visible_alone", #name db field
        "title"=>"Visible individualmente",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
// full form
$model_form = array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"number",
        "name"=>"price", #name db field
        "title"=>"Precio ( eur )",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"place", #name db field
        "title"=>"Place",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"select",
        "select_options" =>array(
            "always" => "Fija",
            "temporary" => "Temporal",
        ),
        "name"=>"type_times", #name db field
        "title"=>"Tipo de book",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

//    array(
//        "type"=>"separator",
//        "title"=>"Fechas",
//    ),
//
//    array(
//        "type"=>"date",
//        "name"=>"date_ini", #name db field
//        "title"=>"Fecha",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),
//    array(
//        "type"=>"date",
//        "name"=>"date_end", #name db field
//        "title"=>"End date",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"User name",
//    ),



//    array(
//        "type"=>"bool",
//        "name"=>"all_time", #name db field
//        "title"=>"All time",
//        "required"=>"true",
//        "value"=>"",
//        "description"=>"Name of company",
//    ),

    array(
        "type"=>"separator",
        "title"=>"General",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"general_text", #name db field
        "title"=>"General",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"image",
        "name"=>"general_image", #name db field
        "title"=>"Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"video",
        "name"=>"general_video", #name db field
        "title"=>"Video",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"separator",
        "title"=>"Included",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_txt", #name db field
        "title"=>"Incluyed",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"incluyed_not_txt", #name db field
        "title"=>"Incluyed not",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Options ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"options_txt", #name db field
        "title"=>"Options",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Important ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"important_txt", #name db field
        "title"=>"Important",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"Optional ",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"optional_txt", #name db field
        "title"=>"Optional",
        "required"=>"",
        "value"=>"",
        "description"=>"User name",
    ),

    array(
        "type"=>"separator",
        "title"=>"General ",
    ),

    array(
        "type"=>"image",
        "name"=>"banner_image", #name db field
        "title"=>"Banner Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"image",
        "name"=>"list_image", #name db field
        "title"=>"List Image",
        "required"=>"",
        "value"=>"",
        "description"=>"Name of company",
    ),


//    array(
//        "type"=>"separator",
//        "title"=>"Info Large",
//    ),
//
//
    array(
        "type"=>"separator",
        "title"=>"Config",
    ),


//    array(
//        "type"=>"text",
//        "name"=>"slug", #name db field
//        "title"=>"Slug ***",
//        "required"=>"",
//        "value"=>"",
//        "description"=>"",
//    ),
    array(
        "type"=>"bool",
        "name"=>"visible_alone", #name db field
        "title"=>"Visible individualmente",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-experiences") {
    $element_table_experiences = array(
        "type" => "table",
        "text" => "Experiencias",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "experiences", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Name",
                ),
//                array(
//                    "title" => "Language",
//                ),
//                array(
//                    "title" => "Currency",
//                ),
//                array(
//                    "title" => "Country",
//                ),
                array(
                    "title" => "Active",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_experiences);
}

if ($module_action_code == "ws_experiences") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
//    array_push($columns, array("db" => "lang", "dt" => count($columns)));
//    array_push($columns, array("db" => "currency", "dt" => count($columns)));
//    array_push($columns, array("db" => "country", "dt" => count($columns)));
    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_experiences") {

    $field_controls="";
    foreach($model_form_create as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>Nueva Experiencia</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_experiences" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_apartments") {
    $render_method = "json";
    $id_item = $_GET['id'];

    // get categories
    $query = "select * from apartments ba
left join apartments_experiences_ref bac
	on bac.id_apartment=ba.id and bac.id_book= {$id_item};";
    $res_query = $sql->query($query,true);
    $selected = array();
    $no_selected = array();
    foreach ($res_query as $item) {
        if ($item['bac']['id_book']!== null)
            array_push($selected, $item);
        else
            array_push($no_selected, $item);
    }
//    die();
    $html_selected = <<<HTML
       
HTML;
    // generate html
    foreach ($selected as $item) {
        $html_selected .= <<<HTML
        
         <form method="post">
            <input name="ag_admin_action" value="remove_book_apartment" type="hidden">
            <input name="id_item" value="{$id_item}"  type="hidden">
            <input name="id" value="{$item["bac"]["id"]}" type="hidden">
            <button type="submit" class="btn btn-success"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{$item['ba']["name"]}</button>
         </form>
HTML;
    }

    $html_no_selected = <<<HTML
        
HTML;
    foreach ($no_selected as $item) {
        $html_no_selected .= <<<HTML
        <form method="post">
          <input name="ag_admin_action" value="add_book_apartment" type="hidden">
          <input name="id_item" value="$id_item"  type="hidden">
          <input name="id_ref"  value="{$item['ba']['id']}" type="hidden">
          <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{$item['ba']["name"]}</button>
        </form>
HTML;
    }

    // list categories selected

    // lista categories unselected
    $html = <<< HTML
    <div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <h2>Hospedajes</h2>
        <p>Hospedajes disponibles para este el book</p>
        <hr>
        
        <h4>Hospedajes seleccionados</h4>
        <p>Estos son los hospedajes que apareceran en el book </p>
        {$html_selected}
        <hr>
        <h4>Hospedajes para este book</h4>
        <p>Estos son los hospedajes disponibles para activar en este book  </p>
        {$html_no_selected}
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}

if($values["ag_admin_action"]=="add_book_apartment"){
    $render_method = "json";
    $data=array(
        "id_book"=>$values["id_item"],
        "id_apartment"=>$values["id_ref"],

    );
    $res = $ag_sql->insert("apartments_experiences_ref",$data);
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['data']=$data;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_apartments",{$values['id_item']});
JS;
    }
}

if($values["ag_admin_action"]=="remove_book_apartment"){
    $render_method = "json";
    $res = $ag_sql->delete("apartments_experiences_ref"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.launch_control("form_apartments",{$values['id_item']});
JS;
    }
}


// actions

if ($module_action_code == "form_apartments_2") {
    $render_method = "json";

    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Apartamentos experiencia</h2>
           <p>Solo salen los apartamentos disponibles y no agregados en la ciuidad</p>
           <h3>
                Apartamentos 
           </h3>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_experiences" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if ($module_action_code == "form_edit_experiences") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Experiencia</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_experiences" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_dates_experiences") {
    $render_method = "json";
    $id_item = $_GET['id'];
    $query = "SELECT * FROM {$table} where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];

    foreach($model_dates as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    if($info['type_times']=="always"){
        foreach($model_dates_days as $item){
            $item['value']=$info[$item['name']];
            $html_form = $ag_admin_module->form_generator($item);
            $field_controls.=$html_form;
        }
    }





    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Fechas y horarios experiencia</h2>
            <hr>
            <form method="post">
            {$field_controls}
            
              <input name="ag_admin_action" value="edit_dates_experiences" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_experiences") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Experiencia</h2>
                <hr>
              <input name="ag_admin_action" value="delete_experiences" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_experiences"){


    $data=array(
        "name"=>$values["name"],
        "description"=>$values["description"],
//        "date_ini"=>$values["date_ini"],
//        "date_end"=>$values["date_end"],
        "all_time"=>$values["all_time"],
        "general_text"=>$values["general_text"],
        "incluyed_txt"=>$values["incluyed_txt"],
        "incluyed_not_txt"=>$values["incluyed_not_txt"],
        "options_txt"=>$values["options_txt"],
        "important_txt"=>$values["important_txt"],
        "optional_txt"=>$values["optional_txt"],
        "slug"=>$values["slug"],
        "active"=>$values["active"],
        "active"=>"off",
        "type_times"=>$values["type_times"],

        "visible_alone"=>$values["visible_alone"],
        "place"=>$values["place"],
        "price"=>$values["price"],
    );

    // add images
    $images_files=array(
        'general_image' => 'none',
        'banner_image' => 'none',
        "general_video"=>$values["general_video"],
    );
    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;
    $res = $ag_sql->insert("{$table}",$data);

    $slug = fix_to_slug("{$values["name"]}_e_{$res}");
    $data=array(
        "slug"=>$slug
    );
    $ag_sql->update("{$table}",$data,"id='{$res}'");

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("experiences",{$res});
JS;
    }
}

if($values["ag_admin_action"]=="edit_dates_experiences"){
    $data=array(

        "date_ini"=>$values["date_ini"],
        "date_end"=>$values["date_end"],

        "hour_ini"=>$values["hour_ini"],
        "hour_end"=>$values["hour_end"],

        "available_monday"=>$values["available_monday"],
        "available_tuesday"=>$values["available_tuesday"],
        "available_wednesday"=>$values["available_wednesday"],
        "available_thursday"=>$values["available_thursday"],
        "available_friday"=>$values["available_friday"],
        "available_saturday"=>$values["available_saturday"],
        "available_sunday"=>$values["available_sunday"],

    );

    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("experiences",{$values['id']});
JS;
    }

}
if($values["ag_admin_action"]=="edit_experiences"){

    $data=array(
        "name"=>$values["name"],
        "description"=>$values["description"],
//        "date_ini"=>$values["date_ini"],
//        "date_end"=>$values["date_end"],
        "all_time"=>$values["all_time"],
        "general_text"=>$values["general_text"],

        "incluyed_txt"=>$values["incluyed_txt"],
        "incluyed_not_txt"=>$values["incluyed_not_txt"],
        "options_txt"=>$values["options_txt"],
        "important_txt"=>$values["important_txt"],
        "optional_txt"=>$values["optional_txt"],
        "slug"=>$values["slug"],
        "active"=>$values["active"],
        "visible_alone"=>$values["visible_alone"],
        "place"=>$values["place"],
        "price"=>$values["price"],
        "type_times"=>$values["type_times"],
    );

    // add images
    $images_files=array(
        'general_image' => 'none',
        'banner_image' => 'none',
        "general_video"=>$values["general_video"],
    );

    $processed_images = $ag_admin->save_images($images_files,$_FILES);
    $array_json['processed_images']=$processed_images;
    $array_json['processed_images_']=$images_files;
    $array_json['processed_images__']=$_FILES;

    $data=array_merge($data,$processed_images);

    $array_json['data']=$data;



    $res = $ag_sql->update("{$table}",$data," id = '{$values['id']}'");

    $slug = fix_to_slug("{$values["name"]}_e_{$values['id']}");
    $data=array(
        "slug"=>$slug
    );
    $res = $ag_sql->update("{$table}",$data,"id= '{$values['id']}'");


    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("experiences",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_experiences"){
    $render_method = "json";
    $res = $ag_sql->delete("experiences"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}