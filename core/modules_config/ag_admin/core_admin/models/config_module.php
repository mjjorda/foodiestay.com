<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Config";
$single_name = "Config";
$table = "config"; # for db ( maybe multiple data bases for ref)
$code = "config"; # for urls
$id_item=$_GET['id'];


$variables =  array(
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Title Home",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_description", #name db field
        "title"=>"Description Home",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"image",
        "name"=>"video_home", #name db field
        "title"=>"Video Home",
        "required"=>"false",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"separator",
        "title"=>"Cancelaciones",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"text",
        "name"=>"cancellation_price", #name db field
        "title"=>"Precio Cancelacion",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"cancellation_hours", #name db field
        "title"=>"Horas cancelacion",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"cancellation_days", #name db field
        "title"=>"Dias cancelacion",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"separator",
        "title"=>"Mails",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Nuevo usuario",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Recuperar contraseña",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Confirmar cuenta",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Confirmar reserva",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Cancelar reserva",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"sub-separator",
        "title"=>"Confirmar apartamento",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"sub-separator",
        "title"=>"Cancelar apartamento",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Feedback",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"email_header", #name db field
        "title"=>"Email General",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Asunto",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"separator",
        "title"=>"Textos",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Home",
        "class"=>"col-md-12",
    ),

    array(
        "type"=>"image",
        "name"=>"title_home", #name db field
        "title"=>"Video",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Banner",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"sub-separator",
        "title"=>"Places List",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"title_home", #name db field
        "title"=>"Banner image",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"image",
        "name"=>"title_home", #name db field
        "title"=>"Video",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Banner",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"text",
        "name"=>"title_home", #name db field
        "title"=>"Title list places",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"text",
        "name"=>"Texto list", #name db field
        "title"=>"Texto Principal",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"BOOKS",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto books",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"sub-separator",
        "title"=>"Contact",
        "class"=>"col-md-12",
    ),
    array(
        "type"=>"image",
        "name"=>"title_home", #name db field
        "title"=>"Banner image",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Banner",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),

    array(
        "type"=>"admin_blog",
        "name"=>"title_home", #name db field
        "title"=>"Texto Largo",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
        "class"=>"col-md-6",
    ),




);

if ($module_action_code == "config") {
    // get forms

//    $query = "SELECT * FROM {$table} where id='{$id_item}'";
//    $info = $sql->query($query);
//    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($variables as $item){
//        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    
    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    
    <div class="col-md-10">
        <h1>
            Config
        </h1>
        <select>
                <option name="es">Español</option>
                <option name="en">Ingles</option>
                <option name="ger">Aleman</option>
            </select>
        <form method="post">
            
            <hr>
            <div class="row">
                {$field_controls}
            </div>
            
          <input name="ag_admin_action" value="edit_config_params" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $form_update_list = array(
        "type" => "html",
        "html" => "{$html}"

    );
    array_push($module['elements'], $form_update_list);
}