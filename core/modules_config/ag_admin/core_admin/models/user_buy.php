<?php
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Reservas";
$single_name = "Reserva";
$table = "user_buy"; # for db ( maybe multiple data bases for ref)
$table_view_paid = "user_buy_view_paid";
$table_view_no_paid = "user_buy_view_no_paid";
$table_view_all = "user_buy_view";
$code = "user_buy"; # for urls

/*
select

book.id as id,
book.date_reserve as date_reserve,
book.date_reserve_end as date_reserve_end,
user.user_ref as date_reserve_end

from user_buy,books,user

where user_buy.`book_ref`=books.id
user.id=user.user_ref;
 */

//if ($module_code == "core" &&
//    ($module_action_code == "list-user_buy"
//        || $module_action_code == "list-user_buy_paid_up"
//        || $module_action_code == "list-user_buy_pending"
//        || $module_action_code == "list-user_buy_all"
//    )) {
//    $module['title'] = "Reservas";
//    $module['elements'] = array();
//
//    $element_buttons = array(
//        "type" => "buttons",
//        "class" => "col-md-12 square-btn",
//        "buttons" => [
//            array(
//                "type" => "url",
//                "icon" => "fa fa-thumbs-o-up",
//                "text" => "Reservas para confirmar",
//                "class" => "square-btn",
//                "url" => "core/list-user_buy",
//                "background" => "#9cbb24"
//            ),
//            array(
//                "type" => "url",
//                "icon" => "fa fa-bank",
//                "text" => "Reservas pago pendiente",
//                "class" => "square-btn",
//                "url" => "core/list-user_buy_pending",
//                "background" => "#5bc0de"
//            ),
//
//            array(
//                "type" => "url",
//                "icon" => "fa fa-th-list",
//                "class" => "square-btn",
//                "text" => "Reservas todas",
//                "url" => "core/list-user_buy_all",
//                "background" => "#aaaaaa"
//            ),
//
//
//        ],
//    );
//
//    array_push($module['elements'], $element_buttons);
//}

$html_inners_edit = <<<HTML
    

HTML;


$model_form = array(
    array(
        "type"=>"text",
        "name"=>"code", #name db field
        "title"=>"Id",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"last_name", #name db field
        "title"=>"Apellido",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"email", #name db field
        "title"=>"Email",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"product_text", #name db field
        "title"=>"Reserva",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"select",
        "select_options" =>array(
            "ex" => "Experiencia",
            "fooday" => "Fooday",
            "plan" => "Programa",
        ),
        "name"=>"product_text", #name db field
        "title"=>"Reserva",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"text",
        "name"=>"product_text", #name db field
        "title"=>"Reserva",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"full_price", #name db field
        "title"=>"Precio",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"date_reserve", #name db field
        "title"=>"Fecha Reserva inicio",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"date_reserve_fin", #name db field
        "title"=>"Fecha Reserva inicio",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"country", #name db field
        "title"=>"Pais",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"text",
        "name"=>"city", #name db field
        "title"=>"Ciudad",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),

    array(
        "type"=>"bool",
        "name"=>"city", #name db field
        "title"=>"Aprobar Personas",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"bool",
        "name"=>"city", #name db field
        "title"=>"Cancelación con seguro",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"bool",
        "name"=>"city", #name db field
        "title"=>"Top End Apartamento",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"bool",
        "name"=>"city", #name db field
        "title"=>"Experiencia Privada",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
    array(
        "type"=>"bool",
        "name"=>"city", #name db field
        "title"=>"Precio total pagado",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),


);


$side_buttons=<<<HTML
<div class="col-md-2 left_buttons">
    <div id_form="form_edit_client" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Detalles</div>
    <div id_form="form_edit_actions" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Estados de reserva </div>
</div>
HTML;


if ($module_action_code == "list-user_buy_all") {
    $element_table_client = array(
        "type" => "table",
        "text" => "Todas las reservas",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "client_all", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "ID Reserva",
                ),
                array(
                    "title" => "Nombre Book ",
                ),
                array(
                    "title" => "Fecha Inicio",
                ),
                array(
                    "title" => "Fecha Fin",
                ),
                array(
                    "title" => "Nombre del cliente",
                ),
                array(
                    "title" => "Precio",
                ),
                array(
                    "title" => "Respuesta Banco ",
                ),
                array(
                    "title" => "Estado",
                ),

            ),
        ),
    );

    array_push($module['elements'], $element_table_client);
}

if ($module_action_code == "ws_client_all") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table_view_all}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id","dt" => count($columns)));
    array_push($columns, array("db" => "id","dt" => count($columns)));
    array_push($columns, array("db" => "book_name","dt" => count($columns)));
    array_push($columns, array("db" => "date_reserve","dt" => count($columns)));
    array_push($columns, array("db" => "date_reserve_end","dt" => count($columns)));
    array_push($columns, array("db" => "user_name","dt" => count($columns)));
    array_push($columns, array("db" => "price","dt" => count($columns)));
    array_push($columns, array("db" => "state_pay","dt" => count($columns)));
    array_push($columns, array("db" => "state_reserve","dt" => count($columns)));


    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}
if ($module_action_code == "list-user_buy") {
    $element_table_client = array(
        "type" => "table",
        "text" => "Reservas",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "client_paid_no_confirm", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                "name"=>array(
                    "title" => "Id",
                ),
                "last_name"=>array(
                    "title" => "Book",
                ),
                "last_name"=>array(
                    "title" => "Usuario",
                ),
                "n_id"=>array(
                    "title" => "Fecha",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_client);
}

if ($module_action_code == "ws_client_paid_no_confirm") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table_view_paid}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "book_name", "dt" => count($columns)));
    array_push($columns, array("db" => "user_name", "dt" => count($columns)));
    array_push($columns, array("db" => "date", "dt" => count($columns)));


    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_client") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Reserva</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_company" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_client"||
    $module_action_code == "form_edit_client_paid_no_confirm"||
    $module_action_code == "form_edit_client_pending"||
    $module_action_code == "form_edit_client_all"
) {
    $render_method = "json";
    $id_item = $_GET['id'];

    // get reserve
    $query = "select * from user_buy where id='{$id_item}' ";
    $reserve = $sql->query($query);
    $reserve = $reserve[0];

    // get book
    $query = "select * from books where id='{$reserve['book_ref']}' ";
    $book = $sql->query($query);
    $book = $book[0];


    // get book
    $query = "select * from cities where id='{$reserve['city']}' ";
    $city = $sql->query($query);
    $city = $city[0];

    // get book
    $query = "select * from places where id='{$book['place']}' ";
    $place = $sql->query($query);
    $place = $place[0];

    // get user
    $query = "select * from user where id='{$reserve['user_ref']}' ";
    $user = $sql->query($query);
    $user = $user[0];

    $query = "SELECT * from apartments where id='{$reserve['apartment_ref']}'";
    $info_apartment = $sql->query($query);

    if(count($info_apartment)>0){
        $info_apartment  = $info_apartment[0];
        $html_apartment=<<< HTML
           <hr>
           <h3>Informaci&oacute; Hospedaje</h3>
           <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Id:</th>
                  <td>{$info_apartment['id']}</td>
                </tr>
                <tr>
                  <th scope="row">Nombre</th>
                  <td>{$info_apartment['name']}</td>
                </tr>
              </tbody>
           </table>
HTML;
    }

    $html = <<< HTML
<div class="row">
    {$side_buttons}
    <div class="col-md-10">
        <form method="post">
           <h2>Reserva</h2>
            <hr>
            <h3>Informaci&oacute; reserva</h3>
            <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Id</th>
                  <td>{$reserve['id']}</td>
                </tr>
                <tr>
                  <th scope="row">Fecha reserva</th>
                  <td>{$reserve['date_creation']}</td>
                </tr>
                <tr>
                  <th scope="row">Precio total:</th>
                  <td>{$reserve['final_full_price']}</td>
                </tr>
                
                <tr>
                  <th scope="row">Huespedes:</th>
                  <td>{$reserve['number_persons']}</td>
                </tr>
                </tbody>
            </table>
            
            <hr>
            <h3>Precios</h3>
            <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Seguro:</th>
                  <td>{$reserve['price_cancellation']}</td>
                </tr>
                <tr>
                  <th scope="row">Hospedaje:</th>
                  <td>{$reserve['price_apartment']}</td>
                </tr>
                <tr>
                  <th scope="row">Privado:</th>
                  <td>{$reserve['price_private']}</td>
                </tr>
                <tr>
                  <th scope="row">Book:</th>
                  <td>{$book['price']}</td>
                </tr>
                <tr>
                  <th scope="row"><b>Precio total:</b></th>
                  <td><b>{$reserve['final_full_price']}</b></td>
                </tr>
              </tbody>
            </table>
            
           
           
           <hr>
           <h3>Informaci&oacute; Usuario</h3>
           <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Id:</th>
                  <td>{$user['id']}</td>
                </tr>
                <tr>
                  <th scope="row">Nombre</th>
                  <td>{$user['name']}</td>
                </tr>
                <tr>
                  <th scope="row">Apellido</th>
                  <td>{$user['last_name']}</td>
                </tr>
                <tr>
                  <th scope="row">Email</th>
                  <td>{$user['email']}</td>
                </tr>
                <tr>
                  <th scope="row"><b>Telefono</b></th>
                  <td>{$user['phone']}</td>
                </tr>
              </tbody>
           </table>
           
           <hr>
           <h3>Informaci&oacute; Book</h3>
           <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Id:</th>
                  <td>{$book['id']}</td>
                </tr>
                <tr>
                  <th scope="row">Nombre</th>
                  <td>{$book['name']}</td>
                </tr>
              </tbody>
           </table>
           
           {$html_apartment}
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}


if ($module_action_code == "form_edit_actions") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $id_item = $_GET['id'];

    // get reserve
    $query = "select * from user_buy where id='{$id_item}' ";
    $reserve = $sql->query($query);
    $reserve = $reserve[0];

    // get book
    $query = "select * from books where id='{$reserve['book_ref']}' ";
    $book = $sql->query($query);
    $book = $book[0];


    // apartaments
    $query = "SELECT * from apartments where id='{$reserve['apartment_ref']}'";
    $info_apartment = $sql->query($query);

    $html_apartment='';

    if(count($info_apartment)>0){
        $info_apartment  = $info_apartment[0];

        if($reserve['state_apartment']=="confirmed"){
            // Confirmado
            $html_apartment=<<< HTML
                <p>El alojamiento esta confirmado</p>
           
HTML;

        }
        if($reserve['state_apartment']=="cancelled"){
// Confirmado
            $html_apartment=<<< HTML
                <p>El alojamiento esta cancelado</p>
           
HTML;
        }
        else{
            $html_apartment=<<< HTML
           <table class="table" style="max-width: 400px;">
              <tbody>
                <tr>
                  <th scope="row">Id:</th>
                  <td>{$info_apartment['id']}</td>
                </tr>
                <tr>
                  <th scope="row">Nombre</th>
                  <td>{$info_apartment['name']}</td>
                </tr>
              </tbody>
           </table>
            <hr>
            <form method="post">
              <input name="ag_admin_action" value="confirm_apartment" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-success">Confirmar Alojamiento</button>
            </form>
            <hr>
            <form method="post">
              <input name="ag_admin_action" value="cancel_apartment" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-danger">Cancelar Alojamiento</button>
            </form>
            <hr>
            
HTML;

        }


    }
    else{

        $html_apartment=<<< HTML
            <p>No se selecciono apartamento</p>
HTML;
    }


    $html_state="";
    if($reserve['state']=='confirmed'){
        $html_state="
        <p>Reserva confirmada</p>";
    }
    elseif($reserve['state']=='cancelled'){
        $html_state="
        <p>Reserva Cancelada</p>";
    }
    else{
        $html_state=<<<HTML
        <form method="post">
          <input name="ag_admin_action" value="confirm_buy" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-success">Confirmar Reserva</button>
        </form>
        <hr>
        <form method="post">
          <input name="ag_admin_action" value="cancel_buy" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-danger">Cancelar Reserva</button>
        </form>
HTML;
    }


    $html = <<< HTML
    <div class="row">
        {$side_buttons}
        <div class="col-md-10">
            {$reserve['state']}
            {$reserve['state_apartment']}
            <h2>Estados de reserva para Book</h2>
            <hr>
            {$html_state}
            
            <hr>
            <h2>Estado de reserva para alojamiento</h2>
            {$html_apartment}
            
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if($values["ag_admin_action"]=="confirm_apartment"){

    $data=array(
        "state_apartment"=>"confirmed",
    );

    $res = $ag_sql->update("user_buy",$data," id = '{$values['id']}'");

    // Send email
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("actions",{$values['id']});
JS;
    }
}
if($values["ag_admin_action"]=="cancel_apartment"){

    $data=array(
        "state_apartment"=>"cancelled",
    );

    $res = $ag_sql->update("user_buy",$data," id = '{$values['id']}'");

    // Send email
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("actions",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="confirm_buy"){

    $data=array(
        "state"=>"confirmed",
    );

    $res = $ag_sql->update("user_buy",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("actions",{$values['id']});
JS;
    }
}
if($values["ag_admin_action"]=="cancel_buy"){

    $data=array(
        "state"=>"cancelled",
    );

    $res = $ag_sql->update("user_buy",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("actions",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="new_client"){


    $data=array(
        "title"=>$values["title"],
        "nit"=>$values["nit"],
        "address"=>$values["address"],
        "phone_contact"=>$values["phone_contact"],
        "mail_contact"=>$values["mail_contact"],
    );

    // add images
    $array_json['data']=$data;
    $res = $ag_sql->insert("user_buy",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("client",{$res});
JS;
    }



}
if($values["ag_admin_action"]=="edit_client"){


    $data=array(
        "name"=>$values['name'],
        "last_name"=>$values['last_name'],
        "n_id"=>$values['n_id'],
        "email"=>$values['email'],
    );

    $res = $ag_sql->update("user_buy",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("client",{$values['id']});
JS;
    }
}


if($values["ag_admin_action"]=="delete_client"){
    $render_method = "json";
    $res = $ag_sql->delete("user_buy"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}