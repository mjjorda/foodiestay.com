<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$title = "Ciudades";
$single_name = "Ciudad";
$table = "cities"; # for db ( maybe multiple data bases for ref)
$code = "cities"; # for urls
$id_item=$_GET['id'];



$html_inners_edit = <<<HTML
    <div id_form="form_edit_cities" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</div>
    <!--<div id_form="form_actions_cities" id_item="{$id_item}" class="admin-btn"><i class="fa fa-cogs" aria-hidden="true"></i> Acciones</div>-->
    <!--<div id_form="form_edit_cities" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_cities" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>    

HTML;


$model_form = array(
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Name",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"select_table",
        "table" =>"places",
        "table_title" =>"name",
        "name"=>"country", #name db field
        "title"=>"Pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"temperature", #name db field
        "title"=>"Temperatura promedio",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),


    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Activo",
        "required"=>"true",
        "value"=>"",
        "description"=>"Name of company",
    ),
);
if ($module_action_code == "list-cities") {
    $element_table_cities = array(
        "type" => "table",
        "text" => "Ciudades",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "cities", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Nombre",
                ),
                array(
                    "title" => "Temperatura promedio",
                ),
                array(
                    "title" => "Activo",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_cities);
}

if ($module_action_code == "ws_cities") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
    array_push($columns, array("db" => "temperature", "dt" => count($columns)));

    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_cities") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>New Ciudades</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_cities" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_cities") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM cities where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Ciudades</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_cities" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_actions_cities") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Actions Ciudades</h2>
            <form method="post">
              <input name="ag_admin_action" value="send_change_pass_cities" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Send Email Recover Pass</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_cities") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Ciudades</h2>
                <hr>
              <input name="ag_admin_action" value="delete_cities" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_cities"){


    $data = $ag_admin_module->process_data($model_form,$values);

    $array_json['data']=$data;
    $res = $ag_sql->insert("cities",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("cities",{$res});
JS;


    }



}
if($values["ag_admin_action"]=="edit_cities"){


    $data = $ag_admin_module->process_data($model_form,$values);

    $array_json['data']=$data;

    $res = $ag_sql->update("cities",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("cities",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_cities"){
    $render_method = "json";
    $res = $ag_sql->delete("cities"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}