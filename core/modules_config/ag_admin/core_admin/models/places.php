<?php

// ##############################
// FEEDBACK module admin
// #############################
$ag_admin_module = \xeki\module_manager::import_module("ag_admin");
$ag_config = \xeki\module_manager::import_module("ag_config");
$title = "Places";
$single_name = "Place";
$table = "places"; # for db ( maybe multiple data bases for ref)
$code = "places"; # for urls
$id_item=$_GET['id'];

$html_inners_edit = <<<HTML
    <div id_form="form_edit_places" id_item="{$id_item}" class="admin-btn"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</div>
    <!--<div id_form="form_actions_places" id_item="{$id_item}" class="admin-btn"><i class="fa fa-cogs" aria-hidden="true"></i> Acciones</div>-->
    <!--<div id_form="form_edit_places" id_item="{$id_item}" class="admin-btn"><i class="fa fa-money" aria-hidden="true"></i> Payments</div>-->
    <div id_form="form_delete_places" id_item="{$id_item}" class="admin-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete </div>    

HTML;


$model_form = array(
    array(
        "type"=>"separator",
        "title"=>"Datos b&aacute;sicos",
    ),
    array(
        "type"=>"text",
        "name"=>"name", #name db field
        "title"=>"Nombre",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"lang", #name db field
        "title"=>"Lenguaje",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"temperature", #name db field
        "title"=>"Temperatura",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"sub-separator",
        "title"=>"Moneda",
    ),
    array(
        "type"=>"text",
        "name"=>"currency", #name db field
        "title"=>"Moneda",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    array(
        "type"=>"text",
        "name"=>"currency_code", #name db field
        "title"=>"Codigo de la moneda code",
        "required"=>"true",
        "value"=>"",
        "description"=>"User name",
    ),
    // array(
    //     "type"=>"text",
    //     "name"=>"country", #name db field
    //     "title"=>"Pa",
    //     "required"=>"",
    //     "value"=>"",
    //     "description"=>"",
    // ),
    array(
        "type"=>"text",
        "name"=>"country_name", #name db field
        "title"=>"Nombre del pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"country_code", #name db field
        "title"=>"Codigo de pa&iacute;s",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"separator",
        "title"=>"Datos lista de places",
    ),
    array(
        "type"=>"text",
        "name"=>"slogan", #name db field
        "title"=>"Slogan",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"image",
        "name"=>"collage_image", #name db field
        "title"=>"Image Collage",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"collage_size", #name db field
        "title"=>"Image Size ( 12 4 3 )",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"video",
        "name"=>"video", #name db field
        "title"=>"Video",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"separator",
        "title"=>"Cabezote",
    ),
    array(
        "type"=>"text",
        "name"=>"banner_text", #name db field
        "title"=>"Texto Banner",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"image",
        "name"=>"banner_image", #name db field
        "title"=>"Imagen Banner",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),


    array(
        "type"=>"separator",
        "title"=>"01 Descubre",
    ),
    array(
        "type"=>"text",
        "name"=>"main_title_1", #name db field
        "title"=>"Bloque 1 titulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"main_text_1", #name db field
        "title"=>"Bloque 1 texto",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"main_title_2", #name db field
        "title"=>"Bloque 2 titulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"main_text_2", #name db field
        "title"=>"Bloque 2 texto",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"main_phrase", #name db field
        "title"=>"Frase",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"separator",
        "title"=>"Listado de comidas ",
    ),
    array(
        "type"=>"image",
        "name"=>"items_food_background", #name db field
        "title"=>"Fondo de comidas",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 1",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_1_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_1_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_1_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 2",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_2_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_2_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_2_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 3",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_3_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_3_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_3_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 4",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_4_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_4_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_4_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 5",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_5_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_5_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_5_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Comida 6",
    ),

    array(
        "type"=>"image",
        "name"=>"items_food_6_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"items_food_6_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"items_food_6_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),



    array(
        "type"=>"separator",
        "title"=>"02 Embajador",
    ),
    array(
        "type"=>"txt",
        "name"=>"ambassador_txt", #name db field
        "title"=>"Texto",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"ambassador_name", #name db field
        "title"=>"Nombre",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"ambassador_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"image",
        "name"=>"ambassador_image", #name db field
        "title"=>"Embajador Image",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"ambassador_email", #name db field
        "title"=>"Embajador email",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"separator",
        "title"=>"03 Lo Esencial",
    ),
    array(
        "type"=>"sub-2-separator",
        "title"=>"Esencial 1",
    ),

    array(
        "type"=>"image",
        "name"=>"essentials_1_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"essentials_1_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"essentials_1_sub_title", #name db field
        "title"=>"Subt&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"essentials_1_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Esencial 2",
    ),

    array(
        "type"=>"image",
        "name"=>"essentials_2_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"essentials_2_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"essentials_2_sub_title", #name db field
        "title"=>"Subt&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"essentials_2_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"sub-2-separator",
        "title"=>"Esencial 3",
    ),

    array(
        "type"=>"image",
        "name"=>"essentials_3_image", #name db field
        "title"=>"Imagen",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"text",
        "name"=>"essentials_3_title", #name db field
        "title"=>"T&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"essentials_3_sub_title", #name db field
        "title"=>"Subt&iacute;tulo",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"admin_blog",
        "name"=>"essentials_3_description", #name db field
        "title"=>"Descripci&oacute;n",
        "required"=>"",
        "value"=>"",
        "description"=>"",
        
    ),

    array(
        "type"=>"separator",
        "title"=>"Foodies",
    ),

    array(
        "type"=>"array_json",
        "array_json_data"=>array(
            array(
                "type"=>"text",
                "title"=>"Title",
                "value_name"=>"title",
                "preview"=>true,
            ),
            array(
                "type"=>"image",
                "title"=>"Image",
                "value_name"=>"image",
                "preview"=>true,
            ),
            array(
                "type"=>"admin_blog",
                "title"=>"Description",
                "value_name"=>"description",
                "preview"=>false,
            ),

        ),
        "name"=>"foodies_array", #name db field
        "title"=>"Foodies",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"separator",
        "title"=>"Configuraci&oacute;n",
    ),

    array(
        "type"=>"text",
        "name"=>"slug", #name db field
        "title"=>"Slug",
        "required"=>"",
        "value"=>"",
        "description"=>"",
    ),
    array(
        "type"=>"text",
        "name"=>"order_list", #name db field
        "title"=>"Priority ( 1,2,3,4  ) ",
        "required"=>true,
        "value"=>"",
        "description"=>"",
    ),

    array(
        "type"=>"bool",
        "name"=>"active", #name db field
        "title"=>"Active",
        "required"=>"true",
        "value"=>"",
        "description"=>"",
    ),
);
if ($module_action_code == "list-places") {
    $element_table_places = array(
        "type" => "table",
        "text" => "Lugares",
        "class" => "col-md-12",
        "table" => array(
            "type" => "table",
            "items_query_code" => "places", # code like ws_
            "background" => "#66ccff",
            "data_fields" => array(
                array(
                    "title" => "Nombre",
                ),
                array(
                    "title" => "Lenguaje",
                ),
                array(
                    "title" => "Moneda",
                ),
                array(
                    "title" => "País",
                ),
                array(
                    "title" => "Activo",
                ),
            ),
        ),
    );

    array_push($module['elements'], $element_table_places);
}

if ($module_action_code == "ws_places") {
//    d($_GET);
    $render_method = "json";
    $table = "{$table}";
    $primaryKey = 'id';
    $columns = array();
    array_push($columns, array("db" => "id", "dt" => count($columns)));
    array_push($columns, array("db" => "name", "dt" => count($columns)));
    array_push($columns, array("db" => "lang", "dt" => count($columns)));
    array_push($columns, array("db" => "currency", "dt" => count($columns)));
    array_push($columns, array("db" => "country", "dt" => count($columns)));
    array_push($columns, array("db" => "active", "dt" => count($columns)));

    $array_json = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
}



if ($module_action_code == "form_new_places") {

    $field_controls="";
    foreach($model_form as $item){
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $render_method = "json";
    $html = <<< HTML
    
<form method="post" enctype="multipart/form-data">
    <h2>Nuevo Place</h2>
    <hr>
    {$field_controls}
  
  <input name="ag_admin_action" value="new_places" type="hidden">
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}
if ($module_action_code == "form_edit_places") {
    $render_method = "json";

    $id_item = $_GET['id'];
    $query = "SELECT * FROM places where id='{$id_item}'";
    $info = $sql->query($query);
    $info = $info[0];
//    d($info);

    $field_controls="";
    foreach($model_form as $item){
        $item['value']=$info[$item['name']];
        $html_form = $ag_admin_module->form_generator($item);
        $field_controls.=$html_form;
    }

    $selected_begin =  $info['position']=="begin_body"?"selected":"";

    $bi_active_html = $info['bi_active'] == "on" ? "checked" : '';
    $html = <<< HTML
<div class="row">
    <div class="col-md-2 left_buttons">
        {$html_inners_edit}
    </div>
    <div class="col-md-10">
        <form method="post">
           <h2>Editar Place</h2>
            <hr>
            {$field_controls}
            
          <input name="ag_admin_action" value="edit_places" type="hidden">
          <input name="id" value="{$id_item}" type="hidden">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );

}

if ($module_action_code == "form_actions_places") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <h2>Actions Place</h2>
            <form method="post">
              <input name="ag_admin_action" value="send_change_pass_places" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">Send Email Recover Pass</button>
            </form>
            <hr>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}
if ($module_action_code == "form_delete_places") {
    $render_method = "json";
    $id_item = $_GET['id'];

    $html = <<< HTML
    <div class="row">
        <div class="col-md-2 left_buttons">
            {$html_inners_edit}
        </div>
        <div class="col-md-10">
            <form method="post">
               <h2>Delete Place</h2>
                <hr>
              <input name="ag_admin_action" value="delete_places" type="hidden">
              <input name="id" value="{$id_item}" type="hidden">
              <button type="submit" class="btn btn-primary">DELETE</button>
            </form>
        </div>
    </div>
HTML;

    $array_json = array(
        "type" => "form",
        "html" => $html,
        "" => "",
        "" => "",
    );
}



if($values["ag_admin_action"]=="new_places"){

    $data = $ag_admin_module->process_data($model_form,$values);

    $array_json['data']=$data;
    $res = $ag_sql->insert("places",$data);

    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("places",{$res});
JS;


    }



}
if($values["ag_admin_action"]=="edit_places"){


    $data = $ag_admin_module->process_data($model_form,$values);
//    d($data);

    $array_json['data']=$data;


    $res = $ag_sql->update("places",$data," id = '{$values['id']}'");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.edit_item("places",{$values['id']});
JS;
    }
}

if($values["ag_admin_action"]=="delete_places"){
    $render_method = "json";
    $res = $ag_sql->delete("places"," id = {$values['id']}");
    if(!$res){
        $array_json['error']=$ag_sql->error();
    }
    else{
        $array_json['id_item']=$res;
        $array_json['callback']= <<<JS
        js_admin.close();
JS;
    }
}