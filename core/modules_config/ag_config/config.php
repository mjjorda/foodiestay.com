<?php

$MODULE_DATA_CONFIG = array(
    "main" => array(
        "type" => "boostrap", // messiJs, magnific-popup,boostrap,others
        "table" =>"ag_config",
        "table_languages" =>"ag_config_languages",
        "languages"=>true,

        "box_items" => array(
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Title Home",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_description", #name db field
                "title"=>"Description Home",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"video",
                "name"=>"video_home", #name db field
                "title"=>"Video Home",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"separator",
                "title"=>"Cancelaciones",
                "class"=>"col-md-12",
            ),
            // array(
            //     "type"=>"text",
            //     "name"=>"cancellation_price", #name db field
            //     "title"=>"Precio Cancelacion",
            //     "required"=>"",
            //     "value"=>"",
            //     "description"=>"",
            //     "class"=>"col-md-6",
            // ),
            array(
                "type"=>"text",
                "name"=>"cancellation_hours", #name db field
                "title"=>"Horas cancelacion",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"cancellation_days", #name db field
                "title"=>"Dias cancelacion",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Menu",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_about", #name db field
                "title"=>"About",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_places", #name db field
                "title"=>"Places",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_book", #name db field
                "title"=>"Book",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_contact", #name db field
                "title"=>"Contact",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_login", #name db field
                "title"=>"Login",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_login", #name db field
                "title"=>"Logout",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_or", #name db field
                "title"=>"Text OR",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_sign_up", #name db field
                "title"=>"Sign up",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_Logout", #name db field
                "title"=>"Logout",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"Sub menu my account",
                "class"=>"col-md-12",
            ),

            array(
                "type"=>"text",
                "name"=>"menu_txt_account", #name db field
                "title"=>"My Account",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_profile", #name db field
                "title"=>"My Profile",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_profile_sub_text", #name db field
                "title"=>"My Profile (sub text)",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_bookings", #name db field
                "title"=>"My bookings",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_bookings_sub_text", #name db field
                "title"=>"My bookings (sub text)",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_favorites", #name db field
                "title"=>"My favorites",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"menu_txt_favorites_sub_text", #name db field
                "title"=>"My favorites (sub text)",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Footer",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"text",
                "name"=>"footer_txt_big", #name db field
                "title"=>"First Text",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"footer_txt_sign_up", #name db field
                "title"=>"Sign up newsletter",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"footer_txt_input_box", #name db field
                "title"=>"Text newsletter",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"footer_txt_btn_newsletter", #name db field
                "title"=>"Button newsletter",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"footer_txt_copy_right", #name db field
                "title"=>"Copyright",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Home",
                "class"=>"col-md-12",
            ),

            array(
                "type"=>"image",
                "name"=>"home_image_banner", #name db field
                "title"=>"Banner image",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"image",
                "name"=>"home_video_banner", #name db field
                "title"=>"Video",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"text",
                "name"=>"home_txt_banner", #name db field
                "title"=>"Texto Banner",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"text",
                "name"=>"home_txt_btn_banner", #name db field
                "title"=>"Button Banner",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"List books",
                "class"=>"col-md-12",
            ),

            array(
                "type"=>"text",
                "name"=>"home_txt_books_btn", #name db field
                "title"=>"See more",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"home_txt_books_btn_all", #name db field
                "title"=>"See all",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"Foodiers",
                "class"=>"col-md-12",
            ),

            array(
                "type"=>"text",
                "name"=>"home_txt_foodiers_title", #name db field
                "title"=>"Foodiers title",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"List places",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"text",
                "name"=>"home_txt_places", #name db field
                "title"=>"Title list places",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Places",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"text",
                "name"=>"places_txt_banner", #name db field
                "title"=>"Text Banner",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"image",
                "name"=>"place_img_banner",
                "title"=>"Image Banner",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"image",
                "name"=>"place_video",
                "title"=>"Video",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_txt_title", #name db field
                "title"=>"Title list places",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_txt_text", #name db field
                "title"=>"Text list places",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"separator",
                "title"=>"View Places",
                "class"=>"col-md-12",
            ),            
            array(
                "type"=>"text",
                "name"=>"places_view_txt_01_title", #name db field
                "title"=>"Discover",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_01_text", #name db field
                "title"=>"Text Discover",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"text",
                "name"=>"places_view_txt_02_title", #name db field
                "title"=>"Ambassador",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_02_text", #name db field
                "title"=>"Text Ambassador",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_03_title", #name db field
                "title"=>"Essentials",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_03_text", #name db field
                "title"=>"Text Essentials",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_04_title", #name db field
                "title"=>"Foodies",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_04_text", #name db field
                "title"=>"Text Foodies",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_04_text_2", #name db field
                "title"=>"Text Foodies 2",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_05_title", #name db field
                "title"=>"Comming",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_05_text", #name db field
                "title"=>"Text Comming",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"places_view_txt_see_more", #name db field
                "title"=>"Txt See More",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Books",
                "class"=>"col-md-12",
            ), 
            array(
                "type"=>"text",
                "name"=>"books_txt_banner_text", #name db field
                "title"=>"Banner Text",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Search",
                "class"=>"col-md-12",
            ), 
            array(
                "type"=>"text",
                "name"=>"books_txt_search_all", #name db field
                "title"=>"All",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_search_date", #name db field
                "title"=>"Date",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_search_country", #name db field
                "title"=>"Country",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_search_price", #name db field
                "title"=>"Price",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_search_duration", #name db field
                "title"=>"Duration",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_search_input", #name db field
                "title"=>"Text box",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Box book",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_save", #name db field
                "title"=>"Button save",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_remove", #name db field
                "title"=>"Button Remove",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_share", #name db field
                "title"=>"Button share",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_price", #name db field
                "title"=>"Text price",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_opinion", #name db field
                "title"=>"Text opinion",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"books_txt_button_see", #name db field
                "title"=>"Button see details",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"Contact",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"contact_img_banner", #name db field
                "title"=>"Banner image",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"text",
                "name"=>"contact_txt_banner", #name db field
                "title"=>"Texto Banner",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"text",
                "name"=>"contact_txt_text1", #name db field
                "title"=>"Texto Largo",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

        ),
        "other"=>array(
            array(
                "type"=>"separator",
                "title"=>"Mails",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Nuevo usuario",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Recuperar contraseña",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Confirmar cuenta",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Confirmar reserva",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Cancelar reserva",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"Confirmar apartamento",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"sub-separator",
                "title"=>"Cancelar apartamento",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"sub-separator",
                "title"=>"Feedback",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"image",
                "name"=>"email_header", #name db field
                "title"=>"Email General",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"text",
                "name"=>"title_home", #name db field
                "title"=>"Asunto",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto Principal",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            array(
                "type"=>"separator",
                "title"=>"Textos",
                "class"=>"col-md-12",
            ),
            
            array(
                "type"=>"sub-separator",
                "title"=>"BOOKS",
                "class"=>"col-md-12",
            ),
            array(
                "type"=>"admin_blog",
                "name"=>"title_home", #name db field
                "title"=>"Texto books",
                "required"=>"",
                "value"=>"",
                "description"=>"",
                "class"=>"col-md-6",
            ),

            
            
        ),
    ),

);