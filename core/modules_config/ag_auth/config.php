<?php

$MODULE_DATA_CONFIG = array(
    "main" => array(
        //default configuration pages
//        "default_pages" => true,

        //custom configuration pages
        "default_pages" => false,
        "folder_base" => "core/pages",
        "folder_pages" => "/module_auth",


        "mail_recover_pass_route" => "default",
        "subject_recover_pass" => "Recuperar Contraseña.",
        "mail_recover_pass_route" => "core/pages/module_auth/mail/recover_pass.html",

        "confirm_account"=>true, // for custom urls set false
        "confirm_account_route_mail" => "default",
        "confirm_account_route_mail" => "core/pages/module_auth/mail/confirm_account.html",

        "confirm_account_route_url" => "confirm-account",

        //custom configuration email // for cusmon uncomment this 
        // "default_emails" => false,
        // "default_email_base" => "core/pages",

        //default configuration

        // config urls
        "use_module_controllers"=>true, // for custom urls set false
        "logged_page" => "books",
        "login_page_url" => "login",
        "register_page_url" => "register",
        "logout_page_url" => "logout",
        "recover_pass_page_url" => "recover-pass",


        // db_info
        "table_user" => "user",
        "field_id" => "id",
        "field_user" => "email",
        "field_password" => "password",
        "field_recover_code" => "recover_code",
        "table_user_temp" => "customer_temp",
        "temp_field_id" => "id",

        // config facebook
        "facebook_login" => true,  # for activate facebook login
        "facebook_app_id" => "160450984503306",
        "facebook_app_secret" => "462c7b550ca5e4ac001cb4c5af81f008",
        "facebook_auth_page" => "auth_facebook",
        "facebook_call_back_url" => "auth_facebook_callback",

        // encryption
        "encryption_method" => "sha256",
        "ultra_secure" => true,

        // messages text

        "msg_no_valid_user" => "Usuario o contraseña no valida.",
        "msg_new_user" => "Nuevo Usuario creado.",
        "msg_user_exist" => "El usuario ya existe",
        "msg_name_space_no_available_for_user" => "Esta zona no es habilitada para el usuario actual",
        "msg_permission_no_available_for_user" => "Permiso no habilitado para el usuario actual",

    ),
//    "secondary" => array(
//    ),
);